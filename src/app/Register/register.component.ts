import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { DialogService } from "../shared/services/dialog.service";
import { RoleService } from "../roles/services/role.service";
import { UserService } from "../users/services/user.service";

@Component({
    selector: "app-register",
    templateUrl: "./register.component.html",
    styleUrls: ["./register.component.css"],
    providers: [RoleService, UserService]
})
export class RegisterComponent implements OnInit {
    roles: any[] = [];
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    role: string = "";
    users: any = [];

    constructor(router: Router,
        private userservice: UserService,
        private roleservice: RoleService,
        private customDialogService: DialogService) {

        this.router = router;
    }

    ngOnInit() {
        this.roleservice.getRoles().subscribe(roleRes => {
            if (roleRes["isSucceeded"] === true) {
                this.roles = roleRes["data"];
            }
            return this.roles;
        });
    }

    public show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    getAllUsers() {
        this.userservice.getUsers().subscribe(
            userRes => {
                this.users = userRes;
            },
            () => {
                console.error();
            });
    }

    saveUser() {

        this.userservice.addUser(this.model).subscribe(
            userRes => {
                if (userRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    this.getAllUsers();
                    this.customDialogService.alert("Done", "added successfully", false);
                    this.router.navigate(["/home/users"]);

                    setTimeout(() => this.visible = false, 300);
                    location.reload();
                } else {
                    this.customDialogService.alert("Fail", "some thing wrong while you add user", false);
                }
            });
    }
}
