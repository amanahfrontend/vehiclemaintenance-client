import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RepairRequestMaterialsAddEditComponent } from "../../repair-request-materials-list/add-edit/repair-request-materials-add-edit.component";
import { RepairRequestSparePartsListComponent } from "../../repair-request-spare-parts/list/repair-request-spare-parts-list.component";
import { RepairRequestSparePartService } from "../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { RepairRequestService } from "../../repair-requests/services/repair-request.service";
import { CommonService } from "../../shared/services/common.service";
import { SparePart } from "../../spare-parts/models/spare-part.model";
import { SparePartService } from "../../spare-parts/services/spare-part.service";
import { Technician } from "../../technicians/models/technician.model";
import { TechnicianService } from "../../technicians/services/technician.service";

@Component({
  selector: "app-repair-request-details-tabs",
  templateUrl: "./repair-request-details-tabs.component.html",
  styleUrls: ["./repair-request-details-tabs.component.css"],
  providers: [RepairRequestService, TechnicianService, RepairRequestSparePartService,
    SparePartService, CommonService]
})
export class RepairRequestDetailsTabsComponent implements OnInit {

  currentUserRole: string;

  // UI
  visible = false;
  visibleAnimate = false;
  isCollapsedSpare = false;
  isCollapsedMaterials = false;

  // Lookups.
  technicians: Technician[];
  spareParts: SparePart[];

  // models to be added or updated.
  modelSparePart: any = {};

  repairRequestId: number;
  vehicleId: number = 0;
  jobCardId: number = 0;

  allowEdit: boolean;

  editModeSparePart: boolean = false;
  lblAddEditRequestSparePartText: string = "Add spare part";
  btnAddUpdateRequestSparePartText: string = "Add spare part";

  @ViewChild("pendingRepairRequestSpareParts") pendingRepairRequestSpareParts: RepairRequestSparePartsListComponent;
  @ViewChild("activeRepairRequestSpareParts") activeRepairRequestSpareParts: RepairRequestSparePartsListComponent;

  @ViewChild("ctrlRepairRequestMaterialsAddEdit") ctrlRepairRequestMaterialsAddEdit: RepairRequestMaterialsAddEditComponent;

  constructor(
    private readonly dialogService: MdlDialogService,
    private readonly repairRequestSparePartService: RepairRequestSparePartService,
    private readonly technicianService: TechnicianService,
    private readonly sparePartService: SparePartService,
    private readonly repairRequestService: RepairRequestService,
    private readonly route: ActivatedRoute,
    private readonly commonService: CommonService
  ) {
    window.scroll(0, 0);
  }

  ngOnInit() {
    window.scroll(0, 0);

    this.currentUserRole = sessionStorage.getItem("userRole");

    this.route.params.subscribe(params => {
      this.repairRequestId = +params["id"];
    });

    this.getTechnicians();
    this.getSpareParts();
  }

  getTechnicians() {
    // get Technicians.
    this.technicianService.getTechnicians().subscribe(
      responseResult => {

        if (responseResult["isSucceeded"] === true) {
          this.technicians = responseResult["data"];
        }
      });
  }

  getSpareParts() {
    // Get spare Parts
    this.sparePartService.getSpareParts().subscribe(
      responseResult => {
        if (responseResult["isSucceeded"] === true) {
          this.spareParts = responseResult["data"];
        }
      });
  }

  show(): void {
    this.visible = true;
    setTimeout(() => (this.visibleAnimate = true), 100);
  }

  hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => (this.visible = false), 300);
  }

  showDialog(vehicleId, jobCardId, repairRequestId, allowEdit: boolean): void {
    window.scroll(0, 0);

    this.allowEdit = allowEdit;

    this.vehicleId = vehicleId;
    this.jobCardId = jobCardId;
    this.repairRequestId = repairRequestId;

    this.ctrlRepairRequestMaterialsAddEdit.getRepairRequestMaterials(repairRequestId);

    this.visible = true;
    setTimeout(() => (this.visibleAnimate = true), 100);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }
  
  currentUserIsManager() {
    return (this.currentUserRole === "Manager");
  }

  // Spare Parts ...................................................

  openAddModeRepairRequestSparePart(): void {

    this.modelSparePart = {
      repairRequestId: this.repairRequestId,
      technicianStartDate: new Date(),
      technicianEndDate: new Date()
    };

    this.lblAddEditRequestSparePartText = "Add spare part";
    this.btnAddUpdateRequestSparePartText = "Add spare part";
    this.isCollapsedSpare = !this.isCollapsedSpare;

    this.editModeSparePart = false;
  }

  openEditModeRepairRequestSparePart(repairRequestSparePart: any) {
    Object.assign(this.modelSparePart, repairRequestSparePart);

    const repairRequestId = repairRequestSparePart.repairRequestId;
    this.modelSparePart.repairRequestId = repairRequestId;

    this.lblAddEditRequestSparePartText = "Edit spare part";
    this.btnAddUpdateRequestSparePartText = "Update spare part";
    this.isCollapsedSpare = !this.isCollapsedSpare;
    this.editModeSparePart = true;
  }

  validateBeforeSavingRequestSparePart(): boolean {

    if (!this.modelSparePart.technicianStartDate) {
      this.dialogService.alert("Technician start date is required");
      return false;
    }

    if (!this.modelSparePart.technicianEndDate) {
      this.dialogService.alert("Technician end date is required");
      return false;
    }

    if (this.commonService.isDateObjIsString(this.modelSparePart.technicianStartDate)) {

      this.modelSparePart.technicianStartDate = this.commonService.convertStringToDate(
        this.modelSparePart.technicianStartDate);

      this.modelSparePart.technicianEndDate = this.commonService.convertStringToDate(
        this.modelSparePart.technicianEndDate);
    }

    // Ignore seconds.
    this.modelSparePart.technicianStartDate.setSeconds(0);
    this.modelSparePart.technicianEndDate.setSeconds(0);

    if (this.modelSparePart.technicianEndDate.getTime() <= this.modelSparePart.technicianStartDate.getTime()) {
      this.dialogService.alert("Technician End date should be greater than technician start date.");
      return false;
    }

    return true;
  }

  saveRequestSparePart() {

    const isValid = this.validateBeforeSavingRequestSparePart();

    if (!isValid) {
      return;
    }

    if (this.modelSparePart.technicianStartDate) {
      this.modelSparePart.technicianStartDate = this.commonService.getDateTimeAsString(this.modelSparePart.technicianStartDate);
    }

    if (this.modelSparePart.technicianEndDate) {
      this.modelSparePart.technicianEndDate = this.commonService.getDateTimeAsString(this.modelSparePart.technicianEndDate);
    }

    this.modelSparePart.vehicleId = this.vehicleId;
    this.modelSparePart.jobCardId = this.jobCardId;

    const saveRepairRequestSparePart = this.modelSparePart.id > 0
      ? this.repairRequestSparePartService.updateRepairRequestSparePart(this.modelSparePart)
      : this.repairRequestSparePartService.addRepairRequestSparePart(this.modelSparePart);

    saveRepairRequestSparePart.subscribe(
      responseResult => {
        if (responseResult && responseResult.isSucceeded === true) {

          // Show message if the spare part needs an approval.
          if (responseResult.data.rowStatusId === 0) {
            this.dialogService.alert("Spare part added successfully but needs an approval. Spare part now is pending.");
            this.pendingRepairRequestSpareParts.repairRequestId = this.repairRequestId;
          } else {
            this.activeRepairRequestSpareParts.repairRequestId = this.repairRequestId;
          }

          if (!this.editModeSparePart) {
            // If add mode of Repair request spare part, clear the model.
            this.modelSparePart = {
              repairRequestId: this.repairRequestId,
              technicianStartDate: new Date(),
              technicianEndDate: new Date()
            };
          } else {
            this.dialogService.alert("Record updated successfully.");
          }

        } else {
          this.dialogService.alert("Something wrong. " + responseResult.message);
        }
      });
  }
}
