export class SparePart {
    id: number;
    serialNumber: string;
    nameAr: string;
    nameEn: string;
    category: string;
    caseAr: string;
    caseEn: string;
    model: string;
    make: string;
    description: string;
    cost: number;
    unit: string;
}
