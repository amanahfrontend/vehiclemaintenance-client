import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { DialogService } from "../../shared/services/dialog.service";
import { SparePartService } from "../services/spare-part.service";

@Component({
  selector: "app-spare-part-add-edit",
  templateUrl: "./spare-part-add-edit.component.html",
  providers: [SparePartService]
})
export class SparePartAddEditComponent implements OnInit {
  router: Router;
  visible = false;
  visibleAnimate = false;
  title: string = "";
  btnTitle: string = "";
  model: any = {};

  constructor(router: Router,
    private customDialogService: DialogService,
    private sparePartService: SparePartService,
    public dialogRef: MatDialogRef<SparePartAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.router = router;
  }

  ngOnInit() {
    this.setComponentLabels();
    this.getSparePart();
  }

  private setComponentLabels() {

    if (this.data.id) {
      this.title = "Edit Spare Part";
      this.btnTitle = "Update";
    } else {
      this.model.needApprove = false;

      this.title = "Add Spare Part";
      this.btnTitle = "Add";
    }
  }

  closeButtonClick(): void {
    this.dialogRef.close();
  }

  hide(): void {
    this.model = {};
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  private getSparePart() {

    if (this.data.id === 0) {
      return;
    }

    this.sparePartService.getSparePart(this.data.id).subscribe(
      result => {
        this.model = result.data;
      },
      () => {
        console.log("error");
      }
    );
  }

  saveSparePart() {

    if (this.model.id) {
      this.updateSparePart();
    } else {
      this.addSparePart();
    }
  }

  addSparePart() {

    this.sparePartService.addSparePart(this.model).subscribe(
      sparePartRes => {
        this.showMessageAfterSaving(sparePartRes);
      });
  }

  updateSparePart() {

    this.sparePartService.updateSparePart(this.model).subscribe(
      sparePartRes => {
        this.showMessageAfterSaving(sparePartRes);
      });
  }

  private showMessageAfterSaving(sparePartRes) {
    if (sparePartRes["isSucceeded"] === true) {
      this.visibleAnimate = false;
      setTimeout(() => this.visible = false, 300);
      this.dialogRef.close();
    } else {

      const errorMessage = this.formulateErrorMessage(sparePartRes);
      this.customDialogService.alert("Fail", errorMessage, false);
    }
  }

  private formulateErrorMessage(responseResult) {
    let errorMessage = "Failed to save the record. ";

    if (responseResult.statusCode !== 0) {
      errorMessage += responseResult.message;
    } else {
      console.log(responseResult.message);
    }

    return errorMessage;
  }
}
