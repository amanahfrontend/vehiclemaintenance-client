import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { SparePartStatus } from "../models/spare-part-status.model";

@Injectable()
export class SparePartStatusService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getSparePartStatus(): Observable<SparePartStatus[]> {
    const url = this.config.baseUrl + "SparePartStatus/Get";

    return this.http.get(url).pipe(map(data => {
      let result = <SparePartStatus[]>data;
      return result;
    }));
  }

  addSparePartStatus(model): Observable<SparePartStatus> {
    model.fK_Status_ID = 1;
    const url = this.config.baseUrl + "SparePartStatus/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        let result = <SparePartStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateSparePartStatus(model): Observable<SparePartStatus> {
    model.fK_Status_ID = 2;
    const url = this.config.baseUrl + "SparePartStatus/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        let result = <SparePartStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteSparePartStatus(id) {
    const url = this.config.baseUrl + "SparePartStatus/Delete/" + id;

    return this.http.delete(url).pipe(map(
      data => {
        let result = <SparePartStatus>data;

        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
