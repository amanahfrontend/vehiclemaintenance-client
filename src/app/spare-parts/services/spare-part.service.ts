import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { SparePart } from "../models/spare-part.model";

@Injectable()
export class SparePartService {
  constructor(private http: HttpClient, private config: WebApiService) { }

  getSpareParts(): Observable<SparePart[]> {
    const url = this.config.baseUrl + "SparePart/Get";

    return this.http.get(url).pipe(map(data => {
      const results = <SparePart[]>data;
      return results;
    }));
  }

  public getSparePart(id: number): Observable<any> {
    const url = this.config.baseUrl + "SparePart/Get/" + id;

    return this.http.get(url).pipe(map(
      data => {
        let result = <SparePart>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  addSparePart(model): Observable<SparePart> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "SparePart/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const results = <SparePart>data;
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  updateSparePart(model): Observable<SparePart> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "SparePart/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const results = <SparePart>data;
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  deleteSparePart(id) {
    let url = "{0}SparePart/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const results = <SparePart>data;
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  searchInventory(name) {
    const url = this.config.baseUrl + "Inventory/Search/" + name;

    return this.http.get(url).pipe(map(
      data => {
        const results = <SparePart>data;
        console.log(results);
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  // searchParams: { name: "", serialNo: "", category: ""}
  searchSpareParts(searchParams): Observable<SparePart[]> {

    const staticUrl = "{0}SparePart/Search?name={1}&serialNo={2}&category={3}";

    const url = staticUrl.replace("{0}", this.config.baseUrl).replace("{1}", searchParams.name)
      .replace("{2}", searchParams.serialNo).replace("{3}", searchParams.category);

    return this.http.get(url).pipe(map(data => {
      const results = <SparePart[]>data;
      return results;
    }));
  }
}
