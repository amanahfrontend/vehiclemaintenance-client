import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { RepairRequestService } from "../services/repair-request.service";
import { JobCardStatus } from "../../job-cards/models/job-card-status.model";

@Component({
    selector: "app-repair-requests-pending-details",
    templateUrl: "./repair-requests-pending-details.component.html",
    providers: [RepairRequestService]
})
export class RepairRequestsPendingDetailsComponent implements OnInit {
    repairRequests: any = [];
    id: number;
    model: any = {};
    repairRequestId: number;
    showaddRepairRequest: boolean;
    status: JobCardStatus;
    isActive: boolean = false;

    constructor(private router: Router,
        private dialogService: MdlDialogService,
        private repairRequestservice: RepairRequestService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.isActive = true;

        this.route.params.subscribe(params => {
            this.id = +params["id"];
            this.repairRequestId = this.id;
        });

        this.getNonApproveRepair();
    }

    getNonApproveRepair() {

        this.repairRequestservice.getRepairRequestById(this.id).subscribe(
            responseResult => {
                if (responseResult["isSucceeded"] === true) {
                    this.repairRequests = responseResult["data"];
                    this.model = responseResult["data"];
                    this.isActive = false;
                }
            });
    }

    delete() {
        const question = "Are you sure, you want to delete this?";
        const result = this.dialogService.confirm(question, "No", "Yes");

        result.subscribe(() => {

            this.repairRequestservice.deleteRepairRequest(this.id).subscribe(
                repairrequestRes => {
                    if (repairrequestRes["isSucceeded"] === true) {
                        this.router.navigateByUrl("/home/repair-requests/pending/list");
                    }
                });
        });
    }

    approve() {

        if (this.model.external) {
            this.model.repairRequestStatusId = 2;// Under Quotation
        }
        else {
            this.model.repairRequestStatusId = 5;// In Progress
        };

        this.repairRequestservice.updateRepairRequest(this.model).subscribe(
            repairrequestRes => {

                if (repairrequestRes["isSucceeded"] === true) {
                    const result = this.dialogService.alert("Approved Successfully");
                    result.subscribe(() => console.log("success"));
                    this.router.navigateByUrl("/home/repair-requests/pending/list");
                }
                else {
                    this.dialogService.alert("some thing wrong");
                }
            });
    }
}

