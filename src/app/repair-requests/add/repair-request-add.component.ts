import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ServiceType } from "../../service-types/models/service-type.model";
import { DriverService } from "../../drivers/services/driver.service";
import { RepairRequestType } from "../../repair-request-type/models/repair-request-type.model";
import { RepairRequestTypeService } from "../../repair-request-type/services/repair-request-type.service";
import { RepairRequestService } from "../../repair-requests/services/repair-request.service";
import { ServiceTypeService } from "../../service-types/services/service-type.service";
import { CommonVariables } from "../../shared/services/common-variables.service";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { Technician } from "../../technicians/models/technician.model";
import { TechnicianService } from "../../technicians/services/technician.service";
import { IVendor } from "../../vendors/models/IVendor.interface";

@Component({
  selector: "app-repair-request-add",
  templateUrl: "./repair-request-add.component.html",
  providers: [
    RepairRequestService, CommonVariables,
    ServiceTypeService, TechnicianService,
    DriverService, RepairRequestTypeService, CommonService
  ]
})
export class RepairRequestAddComponent implements OnInit {
  visible = false;
  visibleAnimate = false;
  model: any = {};
  model2: any = {};
  repairRequestTypes: RepairRequestType[];
  technicians: Technician[];
  serviceTypes: ServiceType[];
  type: boolean;
  vendors: IVendor[];
  date: any = new Date;
  startTime: any = new Date;
  isActive: boolean = false;

  constructor(
    private customDialogService: DialogService,
    private repairRequestTypeService: RepairRequestTypeService,
    private repairRequestService: RepairRequestService,
    private commonS: CommonVariables,
    public dialogRef: MatDialogRef<RepairRequestAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private technicianService: TechnicianService,
    private serviceTypeService: ServiceTypeService,
    private commonService: CommonService) {
  }

  ngOnInit() {

    // Get Service Types
    this.serviceTypeService.getServiceTypes().subscribe(
      serviceTypeRes => {
        if (serviceTypeRes["isSucceeded"] === true) {
          this.serviceTypes = serviceTypeRes["data"];
        }
      });

    // Get repair request types
    this.repairRequestTypeService.getRepairRequestTypes().subscribe(
      repairRequestTypesRes => {
        if (repairRequestTypesRes["isSucceeded"] === true) {
          this.repairRequestTypes = repairRequestTypesRes["data"];
        }
      });

    // Get Technicians
    this.technicianService.getTechnicians().subscribe(
      technicianRes => {
        if (technicianRes["isSucceeded"] === true) {
          this.technicians = technicianRes["data"];
        }
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  show(): void {
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  showDialog(jobCardId): void {
    this.model = {};
    this.commonS.jobCardId = jobCardId;

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  hide(): void {
    this.model = {};
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  saveRepairRequest() {

    this.isActive = true;
    this.model.jobCardId = this.data.jobCardId;

    this.model.userId = sessionStorage.getItem("id");
    this.date = this.commonService.getDateAsString(this.date);

    this.setRepairRequestStatus();

    this.repairRequestService.addRepairRequest(this.model).subscribe(
      repairRequestRes => {
        if (repairRequestRes["isSucceeded"] === true) {
          this.model2.repairRequestId = repairRequestRes["data"].id;

          this.visibleAnimate = false;
          setTimeout(() => this.visible = false, 300);
          this.isActive = false;

          this.dialogRef.close();

        } else {
          this.isActive = false;
          this.customDialogService.alert("Fail", "something wrong", false);
        }
      });
  }

  setRepairRequestStatus() {
    if (this.model.external === "true") {
      this.model.repairRequestStatusId = 1; // Needs approval.
    } else if (this.model.external === "false") {
      this.model.repairRequestStatusId = 5; // In progress.
    }
  }
}
