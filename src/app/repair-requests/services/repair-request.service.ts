import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";

@Injectable()
export class RepairRequestService {
  constructor(private http: HttpClient, private config: WebApiService) { }

  getRepairRequests(): Observable<any> {
    const url = this.config.baseUrl + "RepairRequest/Get";

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  getByMaintnanceType(type): Observable<any[]> {
    const url = this.config.baseUrl + "RepairRequest/GetByMaintenanceType/" + type;

    return this.http.get(url).pipe(map(data => {
      let result = <any[]>data;
      return result;
    }));
  }

  getByExternalandMaintenanceType(external, type): Observable<any[]> {
    const url = this.config.baseUrl + "RepairRequest/GetByExternalandMaintenanceType?external=" + external + "&type=" + type;

    return this.http.get(url).pipe(map(data => {
      let result = <any[]>data;
      return result;
    }));
  }

  addRepairRequest(model): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequest/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error));
      }));
  }

  getRepairRequestById(id): Observable<any> {
    const url = this.config.baseUrl + "RepairRequest/GetRepairRequest/" + id;

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  getByJobCardId(id): Observable<any> {
    const url = this.config.baseUrl + "RepairRequest/GetByJobCardID/" + id;

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  deleteRepairRequest(id): Observable<any> {
    let url = "{0}RepairRequest/Delete/{1}/{2}";
    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id).replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error));
      }));
  }

  updateRepairRequest(model): Observable<any> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequest/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error));
      }));
  }

  getPendingRepairRequests(): Observable<any> {
    const url = this.config.baseUrl + "RepairRequest/GetPendingRepairRequests";

    return this.http.get(url).pipe(map(response => response));
  }

  sparPartsReportRepairRequest(id): Observable<any> {
    const url = this.config.baseUrl + "SparePartsRequest/GetByRepairRequestID/" + id;

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  //materialRequestionReport(id): Observable<RepairRequestSparePart[]> {
  //  const url = this.config.baseUrl + "SparePartsRequest/GetByRepairRequestID2/" + id;

  //  return this.http.get(url).pipe(map(data => {
  //    let result = <RepairRequestSparePart[]>data;
  //    return result;
  //  }));
  //}

  //getAllInternalSpares(): Observable<RepairRequestSparePart[]> {
  //  const url = this.config.baseUrl + "SparePartsRequest/GetAllInternal";

  //  return this.http.get(url).pipe(map(data => {
  //    let result = <RepairRequestSparePart[]>data;
  //    return result;
  //  }));
  //}

  payRequestReport(sparePartRequestId): Observable<any> {
    const url = this.config.baseUrl + "SparePartsRequest/GetById/" + sparePartRequestId;

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  getRepaireRequestWithSearch(query): Observable<any> {
    const urlStatic = "RepairRequest/SearchByID_JobcardID?word={0}";
    const url = this.config.baseUrl + urlStatic.replace("{0}", query);

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }
}
