import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { RepairRequestVendor } from "../models/repair-request-vendor.model";

@Injectable()
export class RepairRequestVendorService {

  constructor(private http: HttpClient, private config: WebApiService) {
  }

  getByRepairRequest(repairRequestId: number): any {
    const url = this.config.baseUrl + "RepairRequestVendor/GetByRepairRequest/" + repairRequestId;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  addRepairRequestVendor(model: RepairRequestVendor): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestVendor/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const results = <any>data;
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  // This delete operation is a physical delete (not soft).
  deleteRepairRequestVendor(id: number) {
    let url = "{0}RepairRequestVendor/Delete/{1}";
    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString());

    return this.http.delete(url).pipe(map(
      data => {
        const response = <any>data;
        return response;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }
}
