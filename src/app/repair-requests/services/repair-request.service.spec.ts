import { TestBed, inject } from '@angular/core/testing';

import { RepairRequestService } from './repair-request.service';

describe('RepairRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RepairRequestService]
    });
  });

  it('should be created', inject([RepairRequestService], (service: RepairRequestService) => {
    expect(service).toBeTruthy();
  }));
});
