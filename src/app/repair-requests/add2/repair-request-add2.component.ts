import { Component, OnInit } from "@angular/core";
import { RepairRequestStatusService } from "../../repair-request-status/services/repair-request-status.service";
import { RepairRequestType } from "../../repair-request-type/models/repair-request-type.model";
import { RepairRequestTypeService } from "../../repair-request-type/services/repair-request-type.service";
import { ServiceType } from "../../service-types/models/service-type.model";
import { ServiceTypeService } from "../../service-types/services/service-type.service";
import { CommonVariables } from "../../shared/services/common-variables.service";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { RepairRequestService } from "../services/repair-request.service";

@Component({
  selector: "app-repair-request-add2",
  templateUrl: "./repair-request-add2.component.html",
  styleUrls: ["./repair-request-add2.component.css"],
  providers: [
    RepairRequestService, CommonVariables, ServiceTypeService,
    RepairRequestStatusService, RepairRequestTypeService, CommonService
  ]
})
export class RepairRequestAdd2Component implements OnInit {
  visible = false;
  visibleAnimate = false;
  model: any = {};
  model2: any = {};
  repairRequestTypes: RepairRequestType[];
  serviceTypes: ServiceType[];
  date: any = new Date;
  startTime: any = new Date;
  isActive: boolean = false;

  constructor(
    private readonly customDialogService: DialogService,
    private readonly repairRequestTypeService: RepairRequestTypeService,
    private readonly repairRequestService: RepairRequestService,
    private readonly commonS: CommonVariables,
    private readonly serviceTypeservice: ServiceTypeService,
    private readonly commonService: CommonService) {

  }

  ngOnInit() {

    // Get Service Types
    this.serviceTypeservice.getServiceTypes().subscribe(
      serviceTypeRes => {
        if (serviceTypeRes["isSucceeded"] === true) {
          this.serviceTypes = serviceTypeRes["data"];
        }
      });

    // Get Repair Request Types
    this.repairRequestTypeService.getRepairRequestTypes().subscribe(
      repairRequestTypesRes => {
        if (repairRequestTypesRes["isSucceeded"] === true) {
          this.repairRequestTypes = repairRequestTypesRes["data"];
        }
      });
  }

  showDialog(jobCardId): void {
    this.model = {};
    this.commonS.jobCardId = jobCardId;

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  hide(): void {
    this.model = {};
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  saveRepairRequest() {

    this.isActive = true;
    this.model.jobCardId = this.commonS.jobCardId;
    this.model.userId = sessionStorage.getItem("id");
    this.date = this.commonService.getDateAsString(this.date);

    this.setRepairRequestStatus();

    this.repairRequestService.addRepairRequest(this.model).subscribe(
      repairRequestRes => {
        if (repairRequestRes["isSucceeded"] === true) {
          this.model2.repairRequestId = repairRequestRes["data"].id;

          this.visibleAnimate = false;
          setTimeout(() => this.visible = false, 300);
          this.isActive = false;

          this.customDialogService.alert("Success", "Saved Successfully", false);

          this.hide();

        } else {
          this.isActive = false;
          this.customDialogService.alert("Fail", "something wrong", false);
        }
      });
  }

  setRepairRequestStatus() {
    if (this.model.external === "true") {
      this.model.repairRequestStatusId = 1; // Needs approval.
    } else if (this.model.external === "false") {
      this.model.repairRequestStatusId = 5; // In progress.
    }
  }
}
