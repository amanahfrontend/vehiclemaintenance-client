import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestsPendingComponent } from './repair-requests-pending.component';

describe('RepairRequestspendingComponent', () => {
    let component: RepairRequestsPendingComponent;
    let fixture: ComponentFixture<RepairRequestsPendingComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RepairRequestsPendingComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RepairRequestsPendingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
