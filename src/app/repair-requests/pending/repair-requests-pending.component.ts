import { Component, OnInit } from "@angular/core";
import { MdlDialogService } from "@angular-mdl/core";
import { RepairRequestService } from "../services/repair-request.service";

@Component({
  selector: "app-repair-requests-pending",
  templateUrl: "./repair-requests-pending.component.html",
  styleUrls: ["./repair-requests-pending.component.css"],
  providers: [RepairRequestService]
})
export class RepairRequestsPendingComponent implements OnInit {
  repairResquests: any = [];
  isActive: boolean = false;

  constructor(private readonly repairRequestService: RepairRequestService,
    private readonly dialogService: MdlDialogService) {
  }

  ngOnInit() {
    this.getPendingRepairRequests();
  }

  getPendingRepairRequests() {
    this.isActive = true;

    this.repairRequestService.getPendingRepairRequests().subscribe(
      repairRequestRes => {
        if (repairRequestRes["isSucceeded"] === true) {
          this.repairResquests = repairRequestRes["data"];
          this.isActive = false;
        } else {
          this.isActive = false;
        }
      });
  }

  approve(model) {
    const question = "Are you sure you want to approve this repair request?";
    const result = this.dialogService.confirm(question, "Later", "Yes");

    // if you need both answers
    result.subscribe(
      () => {

        if (model.external) {
          model.repairRequestStatusId = 2;
        } else {
          model.repairRequestStatusId = 5;
        };

        this.repairRequestService.updateRepairRequest(model).subscribe(
          responseResult => {

            if (responseResult["isSucceeded"] === true) {
              //let result = this.dialogService.alert("updated Successfully");
              //result.subscribe(() => this.getAllRepair());

              this.getPendingRepairRequests();

            } else {
              this.dialogService.alert("Something wrong.");
              console.log(responseResult.message);
            }
          });
      },
      () => {
        console.log("Declined");
      }
    );
  }
}
