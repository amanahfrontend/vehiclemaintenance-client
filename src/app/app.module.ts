import { MdlModule } from "@angular-mdl/core";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatInputModule, MatNativeDateModule, MatRadioModule, MatTabsModule, MAT_DATE_LOCALE } from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChartModule } from "angular-highcharts";
// import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
//import { DashboardModule } from "./dashboard/dashboard.module";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { ChartsModule } from "ng4-charts/ng4-charts";
import { SelectDropDownModule } from "ngx-select-dropdown";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";
import { CannibalizationAddEditComponent } from "./cannibalization/add-edit/cannibalization-add-edit.component";
import { CannibalizationListComponent } from "./cannibalization/list/cannibalization-list.component";
import { DamageMemoAddEditComponent } from "./damage-memos/add-edit/damage-memo-add-edit.component";
import { DamageMemoListComponent } from "./damage-memos/list/damage-memo-list.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DriverAddComponent } from "./drivers/add/driver-add.component";
import { DriverEditComponent } from "./drivers/edit/driver-edit.component";
import { DriversListComponent } from "./drivers/list/drivers-list.component";
import { External_SchRepairRequestComponent } from "./external_schrepairrequest/external_schrepairrequest.component";
import { FileUploaderComponent } from "./file-uploader/file-uploader.component";
import { HomeComponent } from "./home/home.component";
import { Internal_PreRepairRequestComponent } from "./internal_prerepairrequest/internal_prerepairrequest.component";
import { Internal_SchRepairRequestComponent } from "./internal_schrepairrequest/internal_schrepairrequest.component";
import { JobCardRepairRequestSearchCriteriaComponent } from './job-card-repair-request-search-criteria/job-card-repair-request-search-criteria.component';
import { JobCardAddComponent } from "./job-cards/add/job-card-add.component";
import { DailyServiceReportComponent } from "./job-cards/daily-service-report/daily-service-report.component";
import { JobCardDetailsReportComponent } from "./job-cards/details-report/job-card-details-report.component";
import { JobCardsListReportComponent } from './job-cards/list-report/job-cards-list-report.component';
import { JobCardsOpenComponent } from "./job-cards/open/job-cards-open.component";
import { LbdModule } from "./lbd/lbd.module";
import { NotificationsComponent } from "./notifications/notifications.component";
import { preJobCardComponent } from "./prejobcard/prejobcard.component";
import { RegisterComponent } from "./Register/register.component";
import { RepairRequestDetailsTabsComponent } from "./repair-request-details/tabs/repair-request-details-tabs.component";
import { RepairRequestMaterialsAddEditComponent } from "./repair-request-materials-list/add-edit/repair-request-materials-add-edit.component";
import { RepairRequestMaterialsListComponent } from "./repair-request-materials-list/list/repair-request-materials-list.component";
import { RepairRequestSparePartsListComponent } from "./repair-request-spare-parts/list/repair-request-spare-parts-list.component";
import { RepairRequestSparePartsPendingDetailsComponent } from "./repair-request-spare-parts/pending-details/repair-request-spare-parts-pending-details.component";
import { RepairRequestSparePartsPendingComponent } from "./repair-request-spare-parts/pending/repair-request-spare-parts-pending.component";
import { RepairRequestsSparePartsReportComponent } from "./repair-request-spare-parts/report/repair-requests-spare-parts-report.component";
import { RepairRequestStatusAddComponent } from "./repair-request-status/add/repair-request-status-add.component";
import { RepairRequestStatusEditComponent } from "./repair-request-status/edit/repair-request-status-edit.component";
import { RepairRequestStatusListComponent } from "./repair-request-status/list/repair-request-status-list.component";
import { RepairRequestTypeAddComponent } from "./repair-request-type/add/repair-request-type-add.component";
import { RepairRequestTypeEditComponent } from "./repair-request-type/edit/repair-request-type-edit.component";
import { RepairRequestTypeListComponent } from "./repair-request-type/list/repair-request-type-list.component";
import { RepairRequestAddComponent } from "./repair-requests/add/repair-request-add.component";
import { RepairRequestAdd2Component } from "./repair-requests/add2/repair-request-add2.component";
import { RepairRequestEditComponent } from "./repair-requests/edit/repair-request-edit.component";
import { RepairRequestsListTabsComponent } from "./repair-requests/list-tabs/repair-requests-list-tabs.component";
import { RepairRequestsPendingDetailsComponent } from "./repair-requests/pending-details/repair-requests-pending-details.component";
import { RepairRequestsPendingComponent } from "./repair-requests/pending/repair-requests-pending.component";
import { RoleAddComponent } from "./roles/add/role-add.component";
import { RoleEditComponent } from "./roles/edit/role-edit.component";
import { RolesListComponent } from "./roles/list/roles-list.component";
import { schJobCardComponent } from "./schjobcard/schjobcard.component";
import { ServiceTypeAddEditComponent } from "./service-types/add-edit/service-type-add-edit.component";
import { ServiceTypesListComponent } from "./service-types/list/service-types-list.component";
import { ConfirmDialogComponent } from "./shared/components/confirm-dialog/confirm-dialog.component";
import { PaginationComponent } from "./shared/components/pagination/pagination.component";
import { FooterModule } from "./shared/footer/footer.module";
import { NavbarModule } from "./shared/navbar/navbar.module";
import { FilterQueryPipe } from "./shared/pipes/filter-query.pipe";
import { AuthGuard } from "./shared/services/AuthGuard.service";
import { DialogService } from "./shared/services/dialog.service";
import { WebApiService } from "./shared/services/web-api.service";
import { SidebarModule } from "./sidebar/sidebar.module";
import { DeliveryOrderListComponent } from "./spare-parts-delivery-orders/delivery-order-list/delivery-order-list.component";
import { DeliveryOrderNewComponent } from "./spare-parts-delivery-orders/delivery-order-new/delivery-order-new.component";
import { SparePartAddEditComponent } from "./spare-parts/add-edit/spare-part-add-edit.component";
import { SparePartsListComponent } from "./spare-parts/list/spare-parts-list.component";
import { TechnicianAddComponent } from "./technicians/add/technician-add.component";
import { TechnicianEditComponent } from "./technicians/edit/technician-edit.component";
import { TechniciansListComponent } from "./technicians/list/technicians-list.component";
import { TechnicianManpowerComponent } from "./technicians/man-power/technician-man-power.component";
import { TechniciansTabsComponent } from "./technicians/tabs/technicians-tabs.component";
import { TechniciansWorkReportComponent } from "./technicians/work-report/technicians-work-report.component";
import { UpdateattachmentsComponent } from "./updateattachments/updateattachments.component";
import { UserAddComponent } from "./users/add/user-add.component";
import { UserChangePasswordComponent } from "./users/change-password/user-change-password.component";
import { UserEditComponent } from "./users/edit/user-edit.component";
import { UsersListComponent } from "./users/list/users-list.component";
import { LoginComponent } from "./users/login/login.component";
import { UserProfileEditComponent } from "./users/profile-edit/user-profile-edit.component";
import { VehicleTypeAddEditComponent } from "./vehicle-types/add-edit/vehicle-type-add-edit.component";
import { VehicleTypesListComponent } from "./vehicle-types/list/vehicle-types-list.component";
import { VehicleAddEditComponent } from "./vehicles/add-edit/vehicle-add-edit.component";
import { VehicleDetailsTabsComponent } from "./vehicles/details-tabs/vehicle-details-tabs.component";
import { VehicleDetailsComponent } from "./vehicles/details/vehicle-details.component";
import { VehicleDetails2Component } from "./vehicles/details2/vehicle-details2.component";
import { VehiclesListComponent } from "./vehicles/list/vehicles-list.component";
import { VendorAddEditComponent } from "./vendors/add-edit/vendor-add-edit.component";
import { VendorsListComponent } from "./vendors/list/vendors-list.component";

@NgModule(({
  declarations: [
    AppComponent,
    VehicleAddEditComponent,
    JobCardAddComponent,
    RepairRequestAddComponent,
    RepairRequestAdd2Component,
    HomeComponent,
    DriversListComponent,
    DriverAddComponent,
    RoleAddComponent,
    UsersListComponent,
    RegisterComponent,
    LoginComponent,
    NotificationsComponent,
    DashboardComponent,
    ServiceTypeAddEditComponent,
    ServiceTypesListComponent,
    schJobCardComponent,
    RolesListComponent, RoleEditComponent,
    Internal_PreRepairRequestComponent,
    Internal_SchRepairRequestComponent,
    External_SchRepairRequestComponent,
    RepairRequestStatusListComponent,
    RepairRequestEditComponent,
    UpdateattachmentsComponent,
    DriverEditComponent,
    UserEditComponent,
    RepairRequestsPendingDetailsComponent,
    preJobCardComponent,
    RepairRequestSparePartsPendingComponent,
    RepairRequestSparePartsPendingDetailsComponent,
    SparePartAddEditComponent,
    SparePartsListComponent,
    JobCardDetailsReportComponent,
    ConfirmDialogComponent,
    RepairRequestsListTabsComponent,
    FilterQueryPipe,
    TechniciansTabsComponent,
    TechnicianManpowerComponent,
    TechniciansListComponent,
    TechnicianAddComponent,
    TechnicianEditComponent,
    VehicleDetailsTabsComponent,
    DailyServiceReportComponent,
    RepairRequestsSparePartsReportComponent,
    VehicleTypeAddEditComponent,
    VehicleTypesListComponent,
    JobCardsOpenComponent,
    PaginationComponent,
    UserChangePasswordComponent,
    VehicleDetails2Component,
    FileUploaderComponent,
    VendorsListComponent,
    VendorAddEditComponent,
    RepairRequestsPendingComponent,
    RepairRequestStatusAddComponent,
    RepairRequestStatusEditComponent,
    RepairRequestTypeListComponent,
    RepairRequestTypeAddComponent,
    RepairRequestTypeEditComponent,
    UserAddComponent,
    RepairRequestSparePartsListComponent,
    DeliveryOrderNewComponent,
    DeliveryOrderListComponent,
    RepairRequestDetailsTabsComponent,
    UserProfileEditComponent,
    TechniciansWorkReportComponent,
    VehiclesListComponent,
    JobCardRepairRequestSearchCriteriaComponent,
    JobCardsListReportComponent,
    VehicleDetailsComponent,
    RepairRequestMaterialsListComponent,
    RepairRequestMaterialsAddEditComponent,
    CannibalizationListComponent,
    CannibalizationAddEditComponent,
    DamageMemoListComponent,
    DamageMemoAddEditComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NavbarModule,
    FooterModule,
    SidebarModule,
    RouterModule,
    AppRoutingModule,
    MdlModule,
    LbdModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatTabsModule,
    MatCardModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatRadioModule,
    ChartsModule,
    ChartModule,
    MatDialogModule,
    SelectDropDownModule
  ],

  entryComponents: [
    ConfirmDialogComponent,
    VehicleAddEditComponent,
    JobCardAddComponent,
    RepairRequestAddComponent,
    UserAddComponent,
    DriverAddComponent,
    RepairRequestStatusAddComponent,
    RepairRequestTypeAddComponent,
    RoleAddComponent,
    SparePartAddEditComponent,
    VehicleTypeAddEditComponent,
    TechnicianAddComponent,
    ServiceTypeAddEditComponent,
    VendorAddEditComponent,
    CannibalizationAddEditComponent,
    DamageMemoAddEditComponent
  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: MAT_DATE_LOCALE, useValue: "en-GB" },
    WebApiService, AuthGuard, DialogService
  ],
  bootstrap: [AppComponent]
}) as any)
export class AppModule { }
