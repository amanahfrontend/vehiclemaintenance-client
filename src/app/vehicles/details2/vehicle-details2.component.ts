﻿import { Component, OnInit, Input } from "@angular/core";
import { VehicleService } from "../services/vehicle.service";

@Component({
    selector: "app-vehicle-details2",
    templateUrl: "./vehicle-details2.component.html"
})
export class VehicleDetails2Component implements OnInit {
    @Input() vehicleId: number;
    currentVehicle: any;

    constructor(private vehicleService: VehicleService) {

    }

    ngOnInit(): void {
        this.getVehicleById();
    }

    getVehicleById() {

        this.vehicleService.getVehicleById(this.vehicleId).subscribe(
            result => {
                this.currentVehicle = result["data"];
            },
            () => {
                console.log("error");
            }
        );
    }
}