import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { JobCardStatus } from "../../job-cards/models/job-card-status.model";
import { JobCard } from "../../job-cards/models/job-card.model";
import { JobCardStatusService } from "../../job-cards/services/job-card-status.service";
import { JobCardService } from "../../job-cards/services/job-card.service";
import { RepairRequestAddComponent } from "../../repair-requests/add/repair-request-add.component";
import { DriverService } from "../../drivers/services/driver.service";
import { RepairRequestSparePartService } from "../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { RepairRequestService } from "../../repair-requests/services/repair-request.service";
import { CommonVariables } from "../../shared/services/common-variables.service";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { VehicleService } from "../services/vehicle.service";

@Component({
  selector: "app-vehicle-details-tabs",
  templateUrl: "./vehicle-details-tabs.component.html",
  styleUrls: ["./vehicle-details-tabs.component.css"],
  providers: [
    VehicleService, JobCardService, RepairRequestService,
    RepairRequestSparePartService, CommonVariables,
    JobCardStatusService, DriverService, CommonService
  ]
})
export class VehicleDetailsTabsComponent implements OnInit {
  vehicles: any = [];
  isActive: boolean = false;
  vehiclChecked: any = {};
  checkboxValue: boolean = false;

  vehicleId: number;// Parameter: VehicleId
  jobCardId: number; // Parameter: JobCardId

  vehicleObj: any;

  jobCards: JobCard[];
  repairRequests: any = [];
  repairRequestesClicked: boolean = false;
  usedMaterials: any = [];
  jobCardTechnicians: any = [];
  hideme: any = [];
  repaireRequestSpareParts: any = [];
  repairRequestId: any;
  visible = false;
  visibleAnimate = false;
  query: string = "";
  renderer: any;
  cardObj: any = {};
  repairId: number;
  currentJobCards: any = [];
  inactiveJobs: any = [];
  jobCardStatuses: JobCardStatus;
  drivers: any = [];
  activeJobCardsTitle: string;

  constructor(
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private customDialogService: DialogService,
    private commonS: CommonVariables,
    private jobCardS: JobCardService,
    private repairRequestService: RepairRequestService,
    private dialogService: MdlDialogService,
    private jobCardStatusService: JobCardStatusService,
    public dialog: MatDialog,
    private driverService: DriverService,
    private commonService: CommonService
  ) { }

  ngOnInit() {
    window.scroll(0, 0);

    this.route.params.subscribe(
      params => {
        this.vehicleId = +params["vehicleId"];
        this.jobCardId = +params["jobCardId"];
      });

    this.activeJobCardsTitle = this.jobCardId > 0 ? "Job Card # " + this.jobCardId : "Active Job Cards";

    this.repairId = this.commonS.repairId;

    this.getVehicleById();

    if (this.jobCardId > 0) {
      this.getJobCard();
    } else {
      this.getNonCancelledNonClosedJobCards();
    }

    this.getCancelledOrClosedJobCards();

    //// get ServiceTypes
    //this.serviceTypeService.getServiceTypes().subscribe(
    //    serviceTypeRes => {

    //        if (serviceTypeRes["isSucceeded"] === true) {
    //            this.serviceTypes = serviceTypeRes["data"];
    //        }
    //    });

    // Get job card statuses
    this.jobCardStatusService.getJobCardStatuses().subscribe(
      statusRes => {

        if (statusRes["isSucceeded"] === true) {
          this.jobCardStatuses = statusRes["data"];
        }

      });

    // Get drivers
    this.driverService.getDrivers().subscribe(
      driverRes => {
        if (driverRes["isSucceeded"] === true) {
          this.drivers = driverRes["data"];
        }
      });
  }

  openAddRepairRequestDialog(): void {

    const dialogOptions = {
      width: "650px",
      data: { jobCardId: this.jobCardId, id: 0 }
    };

    let dialogRef = this.dialog.open(RepairRequestAddComponent, dialogOptions);

    dialogRef.afterClosed().subscribe(
      () => {
        this.getRepairRequestsOfJobCard();
      });
  }

  openCard(jobCardObject) {
    this.jobCardId = jobCardObject.id;
    this.cardObj = jobCardObject;

    this.cardObj.dateIn = new Date(this.cardObj.dateIn);
    this.cardObj.dateOut = new Date(this.cardObj.dateOut);
  }

  getVehicleById() {
    this.isActive = true;

    this.vehicleService.getVehicleById(this.vehicleId).subscribe(
      result => {
        this.vehicleObj = result["data"];
        this.isActive = false;
      },
      () => {
        console.log("error");
      }
    );
  }

  //getAllJobCards() {
  //    this.isActive = true;

  //    this.jobCardS.getJobCardsByVehicleId(this.vehicleId).subscribe(
  //        result => {
  //            this.jobCards = result["data"];
  //            this.isActive = false;
  //        },
  //        () => {
  //            console.log("error");
  //        }
  //    );
  //}

  getActiveJobCardsBySearch() {
    this.isActive = true;

    this.jobCardS.getAllActiveJobCardsWithSearch2(this.vehicleId, this.query).subscribe(
      result => {
        this.currentJobCards = result["data"];
        this.isActive = false;
      },
      () => {
        console.log("error");
      }
    );
  }

  getAllInActiveJobCardsBySearch() {
    this.isActive = true;

    this.jobCardS.getAllInactiveJobCardsWithSearch2(this.vehicleId, this.query).subscribe(
      result => {
        this.currentJobCards = result["data"];
        this.isActive = false;
      },
      () => {
        console.log("error");
      }
    );
  }

  getCancelledOrClosedJobCards() {
    this.isActive = true;

    this.jobCardS.getCancelledOrClosedJobCardsByVehicleId(this.vehicleId).subscribe(
      result => {
        this.inactiveJobs = result["data"];
        this.isActive = false;
      },
      () => {
        console.log("error");
      }
    );
  }

  getAllInactiveJobCardsBySearch() {
    this.isActive = true;

    this.jobCardS.getAllInactiveJobCardsWithSearch2(this.vehicleId, this.query).subscribe(
      result => {
        this.inactiveJobs = result["data"];
        this.isActive = false;
      },
      () => {
        console.log("error");
      }
    );
  }

  onClick(item) {
    Object.keys(this.hideme).forEach(h => {
      this.hideme[h] = false;
    });

    this.hideme[item.id] = true;
  }

  getRepairRequestsOfJobCard() {
    this.isActive = true;

    this.repairRequestService.getByJobCardId(this.jobCardId).subscribe(
      repairRequestRes => {
        if (repairRequestRes["isSucceeded"] === true) {
          this.repairRequests = repairRequestRes["data"];
          this.isActive = false;
        } else {
          console.log(repairRequestRes.message);
        }
      });
  }

  public show(): void {
    this.isActive = true;
    this.visible = true;

    setTimeout(() => {
      this.isActive = false;
      this.visibleAnimate = true;
    }, 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => (this.visible = false), 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  //showSpareParts(id) {
  //    this.repairRequestId = id;
  //    this.showModel = true;
  //}

  searchJobCardActive() {
    if (this.query) {
      this.getActiveJobCardsBySearch();
    } else {
      this.getNonCancelledNonClosedJobCards();
    }
  }

  searchJobCardInActive() {
    if (this.query) {
      this.getAllInactiveJobCardsBySearch();
    } else {
      this.getCancelledOrClosedJobCards();
    }
  }

  //// dateObj is a date object or a string.
  //getDateAsString(dateObj: any) {

  //    // dateObj is passed as string. "yyyy-MM-dd".
  //    if (dateObj.length === 10) {
  //        return dateObj;
  //    }

  //    // dateObj is passed as string.
  //    if (dateObj[dateObj.length - 1] === "Z") {
  //        return dateObj.split("T")[0];
  //    }

  //    // dateObj is passed as Date object.
  //    return this.commonService.getDateAsString(dateObj);
  //}

  delete(jobcardId) {
    let result = this.dialogService.confirm("Are you sure, you want to delete this?", "No", "Yes");

    // if you need both answers
    result.subscribe(
      () => {

        this.jobCardS.deleteJobCard(jobcardId).subscribe(
          jobcardRes => {

            if (jobcardRes["isSucceeded"] === true) {
              this.getNonCancelledNonClosedJobCards();
            }
          });
      });
  }

  getJobCard() {

    this.jobCardS.getJobCardById(this.jobCardId).subscribe(
      result => {
        this.currentJobCards = [];
        this.currentJobCards.push(result["data"]);
      },
      () => {
        console.log("error");
      }
    );
  }

  getNonCancelledNonClosedJobCards() {
    this.isActive = true;

    this.jobCardS.getNonCancelledNonClosedJobCards(this.vehicleId).subscribe(
      result => {
        this.currentJobCards = result["data"];
        this.isActive = false;
      },
      () => {
        console.log("error");
      }
    );
  }

  updateJobCard() {

    if (this.cardObj.odometer != null && this.cardObj.odometer <= 0) {
      this.customDialogService.alert("Wrong data", "Odometer should be a positive a value.", false);
      return;
    }

    if (this.cardObj.hourmeter != null && this.cardObj.hourmeter <= 0) {
      this.customDialogService.alert("Wrong data", "Hour meter should be a positive a value.", false);
      return;
    }

    if (this.cardObj.dateIn) {
      this.cardObj.dateIn = this.commonService.getDateTimeAsString(this.cardObj.dateIn);
    }

    if (this.cardObj.dateOut) {
      this.cardObj.dateOut = this.commonService.getDateTimeAsString(this.cardObj.dateOut);
    }

    this.jobCardS.updateJobCard(this.cardObj).subscribe(
      jobcardRes => {

        if (jobcardRes["isSucceeded"] === true) {

          this.customDialogService.alert("Done", "updated successfully", false);
          this.getNonCancelledNonClosedJobCards();
          this.getCancelledOrClosedJobCards();

        } else if (jobcardRes.statusCode !== 0) {
          this.customDialogService.alert("Fail", "Something Wrong.\n " + jobcardRes.message, false);
        }
        else {
          this.customDialogService.alert("Fail", "Something Wrong.", false);
          console.log(jobcardRes.message);
        }
      },
      () => {
        this.customDialogService.alert("Fail", "Something Wrong", false);
      }
    );
  }

  deleteRepairRequest(repairRequestId) {
    const question = "Are you sure, you want to delete this?";
    let result = this.dialogService.confirm(question, "No", "Yes");

    // if you need both answers
    result.subscribe(() => {

      this.repairRequestService.deleteRepairRequest(repairRequestId).subscribe(
        repairRequestRes => {

          if (repairRequestRes["isSucceeded"] === true) {
            this.customDialogService.alert("Done", "Deleted successfully", false);
            this.getNonCancelledNonClosedJobCards();
            this.getRepairRequestsOfJobCard();

          } else {
            this.customDialogService.alert("Fail", "some thing wrong", false);
          }
        });
    });
  }

  printOspRepairRequest(repairRequestId: number) {
    // 8 = OSPWorkOrderReport.rdl
    this.commonService.print2(8, `repairRequestId=${repairRequestId}`);
  }

  allowEditJobCard(jobCard) {
    return jobCard.jobCardStatusId === 2 && // InProgress
      this.commonService.currentUserIsManager;
  }

  jobCardStatusIsInProgress(jobCard) {
    return jobCard.jobCardStatusId === 2; // InProgress
  }
}
