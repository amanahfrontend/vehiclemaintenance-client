import { MdlDialogService } from "@angular-mdl/core";
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DialogService } from "../../shared/services/dialog.service";
import { VehicleTypeService } from "../../vehicle-types/services/vehicle-type.service";
import { VehicleService } from "../services/vehicle.service";

@Component({
    selector: "app-vehicle-add-edit",
    templateUrl: "./vehicle-add-edit.component.html",
    styleUrls: ["./vehicle-add-edit.component.css"],
    providers: [VehicleService, VehicleTypeService]
})
export class VehicleAddEditComponent implements OnInit {
    visible = false;
    visibleAnimate = false;
    model: any = {};
    vehicleTypes: any[] = [];
    title: string = "";
    btnTitle: string = "";
    selected: any;
    typeVehicle: number = 0;

    constructor(
        private vehicleService: VehicleService,
        private vehicleTypeService: VehicleTypeService,
        private dialogService: MdlDialogService,
        private customDialogService: DialogService,
        public dialogRef: MatDialogRef<VehicleAddEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    ngOnInit() {

        this.getVehicleTypes();

        this.setComponentLabels();

        if (this.data.id) {
            this.getVehicle();
        } else {
            this.initOnAddMode();
        }
    }

    private setComponentLabels() {
        if (this.data.id) {
            this.title = "Edit Vehicle";
            this.btnTitle = "Update Vehicle";
        } else {
            this.title = "Add Vehicle";
            this.btnTitle = "Add Vehicle";
        }
    }

    private getVehicleTypes() {
        this.vehicleTypeService.getVehicleTypes().subscribe(
            vehicleTypeRes => {
                if (vehicleTypeRes["isSucceeded"] === true) {
                    this.vehicleTypes = vehicleTypeRes["data"];
                }
            });
    }

    getVehicle() {

        this.vehicleService.getVehicleById(this.data.id).subscribe(
            result => {
                this.model = result.data;
            },
            () => {
                console.log("error");
            }
        );
    }

    private initOnAddMode() {
        this.model.status = "Active";
    }

    private validateBeforeSavingVehicle(): boolean {

        if (!this.model.vehicleTypeId) {
            this.dialogService.alert("Vehicle type is required");
            return false;
        }

        if (!this.model.model) {
            this.dialogService.alert("Vehicle model is required");
            return false;
        }

        if (!this.model.customerName) {
            this.dialogService.alert("Customer name is required");
            return false;
        }

        if (!this.model.plateNumber) {
            this.dialogService.alert("Vehicle plate number is required");
            return false;
        }

        if (!this.model.fleetNumber) {
            this.dialogService.alert("Vehicle fleet number is required");
            return false;
        }

        if (!this.model.chassisNumber) {
            this.dialogService.alert("Vehicle chassis number is required");
            return false;
        }

        return true;
    }

    saveVehicle() {
        const isValid = this.validateBeforeSavingVehicle();

        if (!isValid) {
            return;
        }

        if (this.model.id) {

            this.vehicleService.updateVehicle(this.model).subscribe(
                vehicleRes => this.showMessageAfterSave(vehicleRes));
        } else {

            this.vehicleService.addVehicle(this.model).subscribe(
                vehicleRes => this.showMessageAfterSave(vehicleRes));
        }
    }

    private showMessageAfterSave(vehicleRes): void {

        if (vehicleRes["isSucceeded"] === true) {
            //this.customDialogService.alert("Done", operation + " successfully", false);
            this.visibleAnimate = false;
            setTimeout(() => (this.visible = false), 300);
            this.dialogRef.close();
            this.model = {};
        } else {

            const errorMessage = this.formulateErrorMessage(vehicleRes);
            this.customDialogService.alert("Fail", errorMessage, false);
        }
    }

    private formulateErrorMessage(responseResult) {
        let errorMessage = "Failed to save the record. ";

        if (responseResult.statusCode !== 0) {
            errorMessage += responseResult.message;
        } else {
            console.log(responseResult.message);
        }

        return errorMessage;
    }
}
