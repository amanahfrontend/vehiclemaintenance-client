import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { Vehicle, VehiclesSearchParams } from "../models/vehicle.model";

@Injectable()
export class VehicleService {
  constructor(private http: HttpClient, private config: WebApiService) { }

  // Should remove this method. [not-used]
  getVehicles(): Observable<any> {
    return this.getVehiclesPerPage(0, 10000);
  }

  getVehiclesPerPage(page: number, pageSize: number): Observable<any> {
    const url = this.config.baseUrl + "Vehicle/GetVehiclesPerPage?pageNo=" + page + "&pageSize=" + pageSize;
    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  getVehiclesCount(): Observable<any> {
    const url = this.config.baseUrl + "vehicle/Count";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  addVehicle(model): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Vehicle/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  updateVehicle(model): Observable<any> {
    model.modifiedByUserId = this.config.currentUserId;
    model.rowStatusId = 1;

    const url = this.config.baseUrl + "Vehicle/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  deleteVehicle(id) {
    let url = "{0}Vehicle/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  getVehicleById(id): Observable<any> {
    const url = this.config.baseUrl + "Vehicle/Get/" + id;

    return this.http.get(url).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  getBytype(): Observable<any> {
    const url = this.config.baseUrl + "Vehicle/GetByType/false";
    return this.http.get(url).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  // Search vehicles by FleetNo Or PlateNo Or Model and Status.
  searchVehicles(vehiclesSearchParams: VehiclesSearchParams, pageNo: number, pageSize: number): Observable<Vehicle[]> {
    var plateNo = vehiclesSearchParams.vehiclePlateNo;
    var fleetNo = vehiclesSearchParams.vehicleFleetNo;
    var model = vehiclesSearchParams.model;
    var vehicleStatus = vehiclesSearchParams.vehicleStatus;

    var fixedUrl = this.config.baseUrl + "Vehicle/SearchVehicles";
    var url = this.getSearchVehiclesUrl(fixedUrl, plateNo, fleetNo, model, vehicleStatus);
    url += "&pageNo={0}&pageSize={1}".replace("{0}", pageNo.toString()).replace("{1}", pageSize.toString());

    return this.http.get(url).pipe(map(data => {
      let result = <Vehicle[]>data;
      return result;
    }));
  }

  public searchVehiclesRowsCount(vehiclesSearchParams: VehiclesSearchParams): Observable<Vehicle[]> {
    var plateNo = vehiclesSearchParams.vehiclePlateNo;
    var fleetNo = vehiclesSearchParams.vehicleFleetNo;
    var model = vehiclesSearchParams.model;
    var vehicleStatus = vehiclesSearchParams.vehicleStatus;

    var fixedUrl = this.config.baseUrl + "Vehicle/SearchVehiclesRowsCount";
    var url = this.getSearchVehiclesUrl(fixedUrl, plateNo, fleetNo, model, vehicleStatus);

    return this.http.get(url).pipe(map(data => {
      let result = <any>data;
      return result;
    }));
  }

  private getSearchVehiclesUrl(fixedUrl: string, plateNo: string, fleetNo: string, model: string, vehicleStatus: string) {
    const staticUrl = fixedUrl + "?plateNo={0}&fleetNo={1}&model={2}&vehicleStatus={3}";
    const url = staticUrl.replace("{0}", plateNo).replace("{1}", fleetNo).replace("{2}", model).replace("{3}", vehicleStatus);

    return url;
  }
}
