import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";

@Component({
    selector: "confirm-dialog",
    templateUrl: "./confirm-dialog.component.html"
})
export class ConfirmDialogComponent {

    title: string;
    message: string;
    cancelButton: boolean = true;

    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) {

    }
}