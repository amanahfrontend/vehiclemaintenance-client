export class ResponseResult {
    data?: any | null;
    
    message?: string | null;
    
    statusCode?: number | 0;
    
    isSucceeded?: boolean | false;
}
