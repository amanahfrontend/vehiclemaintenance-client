﻿export class BaseEntity {
    id?: number;
    rowStatusId?: number;
    createdByUserId?: string | null;
    modifiedByUserId?: string | null;
}