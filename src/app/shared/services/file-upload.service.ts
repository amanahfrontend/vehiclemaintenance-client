import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "./web-api.service";

@Injectable()
export class FileUploadService {

  constructor(private readonly http: HttpClient, private readonly config: WebApiService) {
  }

  // Upload files
  postFiles(formFiles: FileList, filePaths: string[]): Observable<any> {

    const endpoint = this.config.baseUrl + "UploadFile/UploadFiles";
    const formData = new FormData();

    for (let i = 0; i < formFiles.length; i++) {
      formData.append("formFiles", formFiles[i]);
      formData.append("filePaths", filePaths[i]);
    }

    return this.http.post(endpoint, formData);
  }

  getUploadedFilesByJobCard(jobCardId: number): Observable<any[]> {
    const url = this.config.baseUrl + "UploadFile/GetUploadedFilesByJobCard/" + jobCardId;

    return this.http.get(url).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }

  deleteFile(id: number) {
    let url = "{0}UploadFile/Delete/{1}";
    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString());

    return this.http.delete(url).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
