import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Observable } from "rxjs";
import { ConfirmDialogComponent } from "../components/confirm-dialog/confirm-dialog.component";

@Injectable()
export class DialogService {

  constructor(private dialog: MatDialog) {

  }

  confirm(title, messages, showCancelButton): Observable<boolean> {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = messages;
    dialogRef.componentInstance.cancelButton = showCancelButton;

    return dialogRef.afterClosed();
  }

  alert(title, message, showCancelButton): Observable<boolean> {
    const dialogAlert = this.dialog.open(ConfirmDialogComponent);
    dialogAlert.componentInstance.title = title;
    dialogAlert.componentInstance.message = message;
    dialogAlert.componentInstance.cancelButton = showCancelButton;

    return dialogAlert.afterClosed();
  }
}
