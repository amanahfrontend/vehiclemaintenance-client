import { Component, OnInit } from "@angular/core";
import { RepairRequestService } from "../repair-requests/services/repair-request.service";

@Component({
  selector: "app-internal-prerepairrequest",
  templateUrl: "./internal_prerepairrequest.component.html",
  styleUrls: ["./internal_prerepairrequest.component.css"],
  providers: [RepairRequestService]
})
export class Internal_PreRepairRequestComponent implements OnInit {
  repairrequests: any[];
  type: boolean;

  constructor(private repairrequestservice: RepairRequestService) { }
  ngOnInit() {

    this.type = true;

    this.repairrequestservice.getByExternalandMaintenanceType(false, this.type).subscribe(
      repairrequestRes => {
        if (repairrequestRes["isSucceeded"] === true) {
          this.repairrequests = repairrequestRes["data"];
        }

      });
  }

}
