import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { DialogService } from "../../shared/services/dialog.service";
import { VehicleTypeService } from "../services/vehicle-type.service";

@Component({
  templateUrl: "./vehicle-type-add-edit.component.html",
  providers: [VehicleTypeService]
})
export class VehicleTypeAddEditComponent implements OnInit {
  router: Router;
  public visible = false;
  public visibleAnimate = false;
  model: any = {};
  title: string;
  btnSaveText: string;

  constructor(
    router: Router,
    private vehicleTypeService: VehicleTypeService,
    private customDialogService: DialogService,
    public dialogRef: MatDialogRef<VehicleTypeAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.router = router;
  }

  ngOnInit() {
    this.setupComponentAddOrEdit(this.data.id);
    this.getVehicleType(this.data.id);
  }

  setupComponentAddOrEdit(id: number) {
    if (id === 0) {
      this.title = "Add vehicle type";
      this.btnSaveText = "Add";
    } else {
      this.title = "Edit vehicle type";
      this.btnSaveText = "Save";
    }
  }

  getVehicleType(id: number) {
    if (id === 0) {
      return;
    }

    this.vehicleTypeService.getVehicleType(id).subscribe(vehicleTypeRes => {
      if (vehicleTypeRes["isSucceeded"] === true) {
        this.model = vehicleTypeRes["data"];
      }
    });
  }

  public show(): void {
    this.model = {};
    this.visible = true;
    setTimeout(() => (this.visibleAnimate = true), 100);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public hide(): void {
    this.model = {};
    this.visibleAnimate = false;
    setTimeout(() => (this.visible = false), 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  saveVehicleType() {
    

    if (this.data.id === 0) {
      this.vehicleTypeService
        .addVehicleType(this.model)
        .subscribe(vehicleTypeRes => {
          this.showMessageAfterSaving(vehicleTypeRes, "added");
        });
    } else {
      this.vehicleTypeService
        .updateVehicleType(this.model)
        .subscribe(vehicleTypeRes => {
          this.showMessageAfterSaving(vehicleTypeRes, "updated");
        });
    }
  }

  private showMessageAfterSaving(vehicleTypeRes: any, message: string) {
    if (vehicleTypeRes["isSucceeded"] === true) {
      this.visibleAnimate = false;
      setTimeout(() => (this.visible = false), 300);
      this.customDialogService.alert("Done", message + " successfully.", false);
      this.dialogRef.close();
    } else {
      this.dialogRef.close();
      this.customDialogService.alert("Failed", "something wrong.", false);
    }
  }

  cancel() {
    this.model = {};
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);

    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
