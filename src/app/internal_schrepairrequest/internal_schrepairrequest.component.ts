import { Component, OnInit } from '@angular/core';
import { RepairRequestService } from '../repair-requests/services/repair-request.service';

@Component({
  selector: 'app-internal-schrepairrequest',
  templateUrl: './internal_schrepairrequest.component.html',
  styleUrls: ['./internal_schrepairrequest.component.css'],
  providers: [RepairRequestService]
})

export class Internal_SchRepairRequestComponent implements OnInit {
  repairrequests: any[];
  type: boolean;
  isActive: boolean = false;

  constructor(private repairrequestservice: RepairRequestService) { }

  ngOnInit() {
    this.isActive = true;

    this.type = false;

    this.repairrequestservice.getByExternalandMaintenanceType(false, this.type).subscribe(
      repairrequestRes => {
        if (repairrequestRes["isSucceeded"] == true) {
          this.repairrequests = repairrequestRes["data"];
          console.log(this.repairrequests);
          this.isActive = false;
        }
        return this.repairrequests;
      });
  }

}
