import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { DialogService } from "../../shared/services/dialog.service";
import { ServiceTypeService } from "../services/service-type.service";

@Component({
    selector: "app-service-type-add-edit",
    templateUrl: "./service-type-add-edit.component.html",
    providers: [ServiceTypeService]
})
export class ServiceTypeAddEditComponent implements OnInit {

    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    title: string;
    btnSaveText: string;

    constructor(
        private serviceTypeService: ServiceTypeService,
        private customDialogService: DialogService,
        private dialogRef: MatDialogRef<ServiceTypeAddEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    ngOnInit() {
        this.setupComponentAddOrEdit(this.data.id);
        this.getServiceType(this.data.id);
    }

    setupComponentAddOrEdit(id: number) {
        if (id === 0) {
            this.title = "Add service type";
            this.btnSaveText = "Add";
        } else {
            this.title = "Edit service type";
            this.btnSaveText = "Save";
        }
    }

    private getServiceType(id: number) {
        if (id === 0) {
            return;
        }

        this.serviceTypeService.getServiceType(id).subscribe(
            serviceTypeRes => {
                if (serviceTypeRes["isSucceeded"] === true) {
                    this.model = serviceTypeRes["data"];
                }
            });
    }

    closeButtonClick(): void {
        this.dialogRef.close();
    }

    saveServiceType() {

        if (this.data.id === 0) {
            this.serviceTypeService.addServiceType(this.model).subscribe(
                serviceTypeRes => {
                    this.showMessageAfterSaving(serviceTypeRes);
                });
        } else {
            this.serviceTypeService.updateServiceType(this.model).subscribe(
                serviceTypeRes => {
                    this.showMessageAfterSaving(serviceTypeRes);
                });
        }
    }

    private showMessageAfterSaving(serviceTypeRes: any) {
        if (serviceTypeRes["isSucceeded"] === true) {
            this.visibleAnimate = false;
            setTimeout(() => (this.visible = false), 300);
            this.dialogRef.close();
        } else {

            const errorMessage = this.formulateErrorMessage(serviceTypeRes);

            this.customDialogService.alert("Fail", errorMessage, false);
        }
    }

    private formulateErrorMessage(responseResult) {
        let errorMessage = "Failed to save the record. ";

        if (responseResult.statusCode !== 0) {
            errorMessage += responseResult.message;
        } else {
            console.log(responseResult.message);
        }

        return errorMessage;
    }
}
