export class ServiceType {
  id: number;
  nameAr: string;
  nameEn: string;
  period: number;
}
