import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { ServiceType } from "../models/service-type.model";

@Injectable()
export class ServiceTypeService {
  constructor(private http: HttpClient, private config: WebApiService) { }

  public getServiceTypes(): Observable<ServiceType[]> {
    const url = this.config.baseUrl + "ServiceType/Get";
    return this.http.get(url).pipe(map(data => {
      const result = <ServiceType[]>data;
      return result;
    }));
  }

  public getServiceType(id: number): Observable<any> {

    const url = this.config.baseUrl + "ServiceType/Get/" + id;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  addServiceType(model): Observable<ServiceType> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "ServiceType/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <ServiceType>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  updateServiceType(model): Observable<ServiceType> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "ServiceType/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <ServiceType>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  deleteServiceType(id: number) {
    let url = "{0}ServiceType/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const result = <ServiceType>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }
}
