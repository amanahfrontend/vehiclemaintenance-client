import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DriversListComponent } from "./drivers/list/drivers-list.component";
import { External_SchRepairRequestComponent } from "./external_schrepairrequest/external_schrepairrequest.component";
import { HomeComponent } from "./home/home.component";
import { Internal_PreRepairRequestComponent } from "./internal_prerepairrequest/internal_prerepairrequest.component";
import { DailyServiceReportComponent } from "./job-cards/daily-service-report/daily-service-report.component";
import { JobCardDetailsReportComponent } from "./job-cards/details-report/job-card-details-report.component";
//import { Internal_SchRepairRequestComponent } from "./internal_schrepairrequest/internal_schrepairrequest.component";
import { JobCardsListReportComponent } from "./job-cards/list-report/job-cards-list-report.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { preJobCardComponent } from "./prejobcard/prejobcard.component";
import { RegisterComponent } from "./Register/register.component";
import { RepairRequestSparePartsPendingDetailsComponent } from "./repair-request-spare-parts/pending-details/repair-request-spare-parts-pending-details.component";
import { RepairRequestSparePartsPendingComponent } from "./repair-request-spare-parts/pending/repair-request-spare-parts-pending.component";
import { RepairRequestsSparePartsReportComponent } from "./repair-request-spare-parts/report/repair-requests-spare-parts-report.component";
import { RepairRequestStatusListComponent } from "./repair-request-status/list/repair-request-status-list.component";
import { RepairRequestTypeListComponent } from "./repair-request-type/list/repair-request-type-list.component";
import { RepairRequestsListTabsComponent } from "./repair-requests/list-tabs/repair-requests-list-tabs.component";
import { RepairRequestsPendingDetailsComponent } from "./repair-requests/pending-details/repair-requests-pending-details.component";
import { RepairRequestsPendingComponent } from './repair-requests/pending/repair-requests-pending.component';
import { RolesListComponent } from "./roles/list/roles-list.component";
import { schJobCardComponent } from "./schjobcard/schjobcard.component";
import { ServiceTypesListComponent } from "./service-types/list/service-types-list.component";
import { AuthGuard } from "./shared/services/AuthGuard.service";
import { DeliveryOrderListComponent } from "./spare-parts-delivery-orders/delivery-order-list/delivery-order-list.component";
import { DeliveryOrderNewComponent } from "./spare-parts-delivery-orders/delivery-order-new/delivery-order-new.component";
import { SparePartsListComponent } from "./spare-parts/list/spare-parts-list.component";
import { TechniciansListComponent } from "./technicians/list/technicians-list.component";
import { TechnicianManpowerComponent } from "./technicians/man-power/technician-man-power.component";
import { TechniciansTabsComponent } from "./technicians/tabs/technicians-tabs.component";
import { TechniciansWorkReportComponent } from "./technicians/work-report/technicians-work-report.component";
import { UsersListComponent } from "./users/list/users-list.component";
import { LoginComponent } from "./users/login/login.component";
import { UserProfileEditComponent } from "./users/profile-edit/user-profile-edit.component";
import { VehicleTypesListComponent } from "./vehicle-types/list/vehicle-types-list.component";
import { VehicleDetailsTabsComponent } from "./vehicles/details-tabs/vehicle-details-tabs.component";
import { VehiclesListComponent } from "./vehicles/list/vehicles-list.component";
import { VendorsListComponent } from "./vendors/list/vendors-list.component";
import { CannibalizationListComponent } from "./cannibalization/list/cannibalization-list.component";
import { DamageMemoListComponent } from "./damage-memos/list/damage-memo-list.component";

//import { JobCardComponent } from "./job-card/job-card.component";
//import { DashboardModule } from "./dashboard/dashboard.module";

// For authorization 'to insure user is for authorization 'to insure user is logging'

const routes: Routes = [
  { path: "login", component: LoginComponent },
  {
    path: "home",
    component: HomeComponent,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent,
        //loadChildren: () => DashboardModule,
        canActivate: [AuthGuard]
      },
      {
        path: "register",
        component: RegisterComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "technician_manpower",
        component: TechnicianManpowerComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/pending/list",
        component: RepairRequestsPendingComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/pending/details/:id",
        component: RepairRequestsPendingDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/spare-parts/pending/list",
        component: RepairRequestSparePartsPendingComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/spare-parts/pending/details/:id",
        component: RepairRequestSparePartsPendingDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/spare-parts/delivery-order/new",
        component: DeliveryOrderNewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/spare-parts/delivery-order/list",
        component: DeliveryOrderListComponent,
        canActivate: [AuthGuard]
      },
      { path: "account/profile/edit", component: UserProfileEditComponent, canActivate: [AuthGuard] },
      { path: "vehicles/list", component: VehiclesListComponent, canActivate: [AuthGuard] },
      { path: "vehicles/details/:vehicleId", component: VehicleDetailsTabsComponent, canActivate: [AuthGuard] },
      { path: "notifications", component: NotificationsComponent, canActivate: [AuthGuard] },
      { path: "schjobcards", component: schJobCardComponent, canActivate: [AuthGuard] },
      { path: "prejobcards", component: preJobCardComponent, canActivate: [AuthGuard] },

      // Settings
      { path: "settings/users/list", component: UsersListComponent, canActivate: [AuthGuard] },
      { path: "settings/technicians/list", component: TechniciansListComponent, canActivate: [AuthGuard] },
      { path: "settings/technicians/tabs", component: TechniciansTabsComponent, canActivate: [AuthGuard] },
      { path: "settings/drivers/list", component: DriversListComponent, canActivate: [AuthGuard] },
      { path: "settings/service-types/list", component: ServiceTypesListComponent, canActivate: [AuthGuard] },
      { path: "settings/spare-parts/list", component: SparePartsListComponent, canActivate: [AuthGuard] },
      { path: "settings/vehicle-types/list", component: VehicleTypesListComponent, canActivate: [AuthGuard] },
      {
        path: "settings/repair-request-statuses/list",
        component: RepairRequestStatusListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "settings/repair-request-types/list",
        component: RepairRequestTypeListComponent,
        canActivate: [AuthGuard]
      },
      { path: "settings/vendors/list", component: VendorsListComponent, canActivate: [AuthGuard] },
      { path: "settings/roles/list", component: RolesListComponent, canActivate: [AuthGuard] },

      // Reports
      { path: "job-cards/report", component: JobCardsListReportComponent, canActivate: [AuthGuard] },
      { path: "job-cards/details/:id", component: JobCardDetailsReportComponent, canActivate: [AuthGuard] },
      {
        path: "job-cards/details/:vehicleId/:jobCardId",
        component: VehicleDetailsTabsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "repair-requests/spare-parts/report",
        component: RepairRequestsSparePartsReportComponent,
        canActivate: [AuthGuard]
      },
      { path: "daily-services/report", component: DailyServiceReportComponent, canActivate: [AuthGuard] },
      { path: "technicians/report", component: TechniciansWorkReportComponent, canActivate: [AuthGuard] },
      { path: "cannibalization/list", component: CannibalizationListComponent, canActivate: [AuthGuard] },
      { path: "damage-memo/list", component: DamageMemoListComponent, canActivate: [AuthGuard] },
      {
        path: "repair-requests/list",
        component: RepairRequestsListTabsComponent,
        canActivate: [AuthGuard]
      },
      //{
      //  path: "repair-requests/details/:id",
      //  component: RepairRequestDetailsTabsComponent,
      //  canActivate: [AuthGuard]
      //},
      {
        path: "repair-requests/internal/list",
        component: Internal_PreRepairRequestComponent,
        canActivate: [AuthGuard]
      },
      //{
      //  path: "internal_schrepairrequests",
      //  component: Internal_SchRepairRequestComponent,
      //  canActivate: [AuthGuard]
      //},
      {
        path: "repair-requests/external/list",
        component: External_SchRepairRequestComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "",
        redirectTo: "login",
        pathMatch: "full",
        canActivate: [AuthGuard]
      }
    ]
  },
  { path: "", redirectTo: "login", pathMatch: "full", canActivate: [AuthGuard] }
];

@NgModule({
  imports: [CommonModule, BrowserModule, RouterModule.forRoot(routes)],
  exports: []
})
export class AppRoutingModule { }
