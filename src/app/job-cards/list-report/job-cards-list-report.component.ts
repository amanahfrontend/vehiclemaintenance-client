import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../shared/services/common.service";
import { JobCardStatusService } from "../services/job-card-status.service";
import { JobCardService } from "../services/job-card.service";

@Component({
    selector: "app-job-cards-list-report",
    templateUrl: "./job-cards-list-report.component.html",
    styleUrls: ["./job-cards-list-report.component.scss"],
    providers: [JobCardService, JobCardStatusService, CommonService]
})
export class JobCardsListReportComponent implements OnInit {
    jobCards: any = [];
    prejobCards: any = [];
    technicians: any = [];
    serviceTypes: any = [];
    jobCardStatuses: any = [];
    jobcardTechnicians: any = [];
    fKJobCardId: number[] = [];
    fKTechnicianId: any;
    startDate: Date;
    endDate: Date;
    isActive: boolean = false;
    type: boolean;
    showNoDataFoundLabel: boolean = false;

    // Search parameters of job card.
    jobCardId: number;
    custType: string = "All";
    serviceTypeId: number = 0;
    jobCardStatusId: number = 0;
    dateIn: Date;
    dateOut: Date;
    vehiclePlateNo: string = "";
    vehicleFleetNo: string = "";

    constructor(
        private jobCardService: JobCardService,
        private jobCardStatusService: JobCardStatusService,
        private commonService: CommonService
    ) {
    }

    ngOnInit() {

        //this.getServiceTypes();
        this.getJobCardStatuses();
    }

    //private getServiceTypes(): void {

    //    this.serviceTypeService.getServiceTypes().subscribe(
    //        serviceTypeRes => {
    //            if (serviceTypeRes["isSucceeded"] === true) {
    //                this.serviceTypes = serviceTypeRes["data"];
    //            }
    //        });
    //}

    private getJobCardStatuses(): void {
        this.jobCardStatusService.getJobCardStatuses().subscribe(
            statusRes => {
                if (statusRes["isSucceeded"] === true) {
                    this.jobCardStatuses = statusRes["data"];
                }
            });
    }

    //getJobCards() {
    //    this.isActive = true;

    //    this.jobCardService.getJobCards().subscribe(
    //        schjobCardRes => {

    //            if (schjobCardRes["isSucceeded"] === true) {
    //                this.jobCards = schjobCardRes["data"];
    //                this.isActive = false;
    //            }
    //        },
    //        () => {
    //            this.isActive = false;
    //        }
    //    );
    //}

    clearSearch() {
        this.dateIn = null;
        this.dateOut = null;
        this.custType = "All";
        this.serviceTypeId = 0;
        this.jobCardStatusId = 0;
        this.vehiclePlateNo = "";
        this.vehicleFleetNo = "";

        this.searchJobCards();
    }

    searchJobCards(): void {
        this.isActive = true;

        var custType = this.custType;

        if (this.custType === "All") {
            custType = "";
        }

        var dateIn;

        if (this.dateIn) {
            dateIn = this.commonService.getDateAsString(this.dateIn);
        } else {
            dateIn = null;
        }

        var dateOut;

        if (this.dateOut) {
            dateOut = this.commonService.getDateAsString(this.dateOut);
        } else {
            dateOut = null;
        }

        var serviceTypeId = this.serviceTypeId;
        var jobCardStatusId = this.jobCardStatusId;

        var vehiclePlateNo = this.vehiclePlateNo;
        var vehicleFleetNo = this.vehicleFleetNo;

        this.jobCardService.searchJobCards(custType, dateIn, dateOut, serviceTypeId,
            jobCardStatusId, vehiclePlateNo, vehicleFleetNo, this.jobCardId).subscribe(
                jobCardRes => {

                    if (jobCardRes["isSucceeded"] === true) {
                        this.jobCards = jobCardRes["data"];
                        this.isActive = false;

                        this.showNoDataFoundLabel = this.jobCards.length === 0;
                    } else {
                        console.log(jobCardRes.message);
                    }
                },
                () => {
                    this.isActive = false;
                }
            );
    }

    printJobCardsListReport() {

        let custName = this.custType;

        if (this.custType === "All") {
            custName = "";
        }

        let dateIn: string;

        if (this.dateIn) {
            dateIn = this.commonService.getDateAsString(this.dateIn);
        } else {
            dateIn = "";
        }

        let dateOut: string;

        if (this.dateOut) {
            dateOut = this.commonService.getDateAsString(this.dateOut);
        } else {
            dateOut = "";
        }

        let serviceTypeId: string;

        if (this.serviceTypeId === 0) {
            serviceTypeId = "";
        } else {
            serviceTypeId = this.serviceTypeId.toString();
        }

        let jobCardStatusId: string;

        if (this.jobCardStatusId === 0) {
            jobCardStatusId = "";
        } else {
            jobCardStatusId = this.jobCardStatusId.toString();
        }

        let jobCardId: string;

        if (this.jobCardId === undefined) {
            jobCardId = "";
        } else {
            jobCardId = this.jobCardId.toString();
        }

        const queryStringParams: string =
            `customerName=${custName}&startDate=${dateIn}&endDate=${dateOut}&serviceTypeId=${serviceTypeId
            }&jobCardStatusId=${jobCardStatusId}&vehiclePlateNo=${this.vehiclePlateNo}&vehicleFleetNo=${this
                .vehicleFleetNo}&jobCardId=${jobCardId}`;

        // 1 = JobCardsListReport.rdl
        this.commonService.print2(1, queryStringParams);
    }

    printJobCardReport(jobCardId: number): void {
        const queryStringParams = `jobCardId=${jobCardId}`;
        this.commonService.print2(2, queryStringParams);
    }
}
