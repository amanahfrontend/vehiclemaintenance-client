import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { JobCard } from "../../job-cards/models/job-card.model";
import { JobCardService } from "../../job-cards/services/job-card.service";
import { RepairRequestSparePartService } from "../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { RepairRequestService } from "../../repair-requests/services/repair-request.service";
import { CommonService } from "../../shared/services/common.service";
import { FileUploadService } from "../../shared/services/file-upload.service";

@Component({
  selector: "app-job-card-details-report",
  templateUrl: "./job-card-details-report.component.html",
  styleUrls: ["./job-card-details-report.component.css"],
  providers: [
    JobCardService, RepairRequestService, CommonService,
    RepairRequestSparePartService, FileUploadService
  ]
})
export class JobCardDetailsReportComponent implements OnInit {

  todayDate: Date = new Date();
  jobCard: JobCard[];
  usedMaterials: any[];
  jobCardTechnicians: any[];
  jobCardId: any = this.route.snapshot.params["id"];
  id: number;
  model: any = {};
  isActive: boolean = false;
  repairRequests: any[];
  spareParts: any[];
  repairRequestIds: any = [];
  uploadedFiles: any[] = [];

  constructor(
    private repairRequestService: RepairRequestService,
    private jobCardService: JobCardService,
    private sparePartsRequestService: RepairRequestSparePartService,
    private commonService: CommonService,
    private fileUploadService: FileUploadService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = +params["id"];
      this.jobCardId = this.id;
    });

    this.getJobCardData();
    this.getRepairRequestsByJobCard();
    this.getSparePartsOfJobCard();
    this.getUploadedFilesOfJobCard();
  }

  getJobCardData() {

    this.jobCardService.getJobCardById(this.id).subscribe(
      jobCardRes => {
        if (jobCardRes["isSucceeded"] === true) {
          this.model = jobCardRes["data"];
          this.isActive = false;
        }
      });
  }

  getRepairRequestsByJobCard() {

    this.repairRequestService.getByJobCardId(this.jobCardId).subscribe(
      repairRequestRes => {
        this.isActive = true;

        if (repairRequestRes["isSucceeded"] === true) {
          this.repairRequests = repairRequestRes["data"];
          this.isActive = false;
        }
      });
  }

  getSparePartsOfJobCard() {

    this.sparePartsRequestService.getRepairRequestSparePartsOfJobCard(this.jobCardId).subscribe(
      repairRequestRes => {
        this.isActive = true;

        if (repairRequestRes["isSucceeded"] === true) {
          this.spareParts = repairRequestRes["data"];
          this.isActive = false;
        }
      });
  }

  getUploadedFilesOfJobCard() {

    this.fileUploadService.getUploadedFilesByJobCard(this.id).subscribe(
      responseResult => {
        if (responseResult["isSucceeded"] === true) {
          this.uploadedFiles = responseResult["data"];
        }
      });
  }

  print(): void {
    const queryStringParams = `jobCardId=${this.jobCardId}`;
    this.commonService.print2(2, queryStringParams);
  }
}
