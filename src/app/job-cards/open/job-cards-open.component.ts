import { Component, OnInit } from "@angular/core";
import { JobCardService } from "../../job-cards/services/job-card.service";

@Component({
  selector: "app-job-cards-open",
  templateUrl: "./job-cards-open.component.html",
  providers: [JobCardService]
})
export class JobCardsOpenComponent implements OnInit {
  openJobCards: any = [];
  isActive: boolean = false;

  constructor(private jobCardService: JobCardService) { }

  ngOnInit() {
    this.getOpenJobCards();
  }

  getOpenJobCards() {
    this.isActive = true;

    this.jobCardService.getOpenJobCards().subscribe(
      schjobCardRes => {

        if (schjobCardRes["isSucceeded"] === true) {
          this.openJobCards = schjobCardRes["data"];
          this.isActive = false;
        }
      },
      () => {
        this.isActive = false;
      }
    );
  }
}
