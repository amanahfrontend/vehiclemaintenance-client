import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { JobCardStatus } from "../models/job-card-status.model";

@Injectable()
export class JobCardStatusService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getJobCardStatuses(): Observable<JobCardStatus[]> {
    const url = this.config.baseUrl + "JobCardStatus/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <JobCardStatus[]>data;
      return result;
    }));
  }

  addJobCardStatus(model): Observable<JobCardStatus> {
    const url = this.config.baseUrl + "JobCardStatus/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <JobCardStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateJobCardStatus(model): Observable<JobCardStatus> {
    const url = this.config.baseUrl + "JobCardStatus/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <JobCardStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteJobCardStatus(id) {
    const url = this.config.baseUrl + "JobCardStatus/Delete/" + id;

    return this.http.delete(url).pipe(map(
      data => {
        const result = <JobCardStatus>data;

        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
