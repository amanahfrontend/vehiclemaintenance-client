import { MdlDialogService } from "@angular-mdl/core";
import { Component, Inject, OnInit, ViewChild } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { CommonService } from "../../shared/services/common.service";
import { VehicleService } from "../../vehicles/services/vehicle.service";
import { DriverService } from "../../drivers/services/driver.service";
import { FileUploaderComponent } from "../../file-uploader/file-uploader.component";
import { JobCardService } from "../services/job-card.service";

@Component({
    selector: "app-job-card-add",
    templateUrl: "./job-card-add.component.html",
    providers: [
        JobCardService, DriverService, VehicleService, CommonService
    ]
})
export class JobCardAddComponent implements OnInit {
    visible = false;
    visibleAnimate = false;
    model: any = {};

    currentVehicle: any = {};

    drivers: any = [];
    vehicles: any = [];
    type: boolean;

    @ViewChild("d") datePicker;
    @ViewChild("fileUploader") fileUploader: FileUploaderComponent;

    date: any = new Date();
    fleetNum: string;
    isValidFormSubmitted = false;
    selectOne: any;

    isActive: boolean = false;
    userId = JSON.parse(localStorage.getItem("userId"));

    constructor(
        private jobCardService: JobCardService,
        private vehicleService: VehicleService,
        private driverService: DriverService,
        private dialogService: MdlDialogService,
        private commonService: CommonService,
        private dialogRef: MatDialogRef<JobCardAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    ngOnInit() {

        this.getDrivers();
        this.getVehicle();
    }

    getVehicle() {
        if (this.data.vehicleId) {

            this.vehicleService.getVehicleById(this.data.vehicleId).subscribe(
                result => {
                    this.currentVehicle = result.data;
                },
                () => {
                    console.log("error");
                }
            );
        }
    }

    private getDrivers(): void {

        // Get drivers.
        this.driverService.getDrivers().subscribe(
            driverRes => {

                if (driverRes["isSucceeded"] === true) {
                    this.drivers = driverRes["data"].filter(obj => obj.active === true);
                }
            });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    show(vehicleId): void {
        this.model = {};

        if (!vehicleId) {
            this.dialogService.alert("Please, pick vehicle first");
        } else {
            this.jobCardService.canAddJobCard(vehicleId).subscribe(
                result => {
                    if (result.data === false) {
                        this.dialogService.alert(result.status.message);
                    } else {
                        this.visible = true;
                        this.model.vehicleId = vehicleId;
                        setTimeout(() => (this.visibleAnimate = true), 100);
                    }
                },
                () => {
                    console.log("error");
                }
            );
        }
    }

    hide(): void {
        this.model = {};
        this.visibleAnimate = false;
        setTimeout(() => (this.visible = false), 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    private validateBeforeSavingJobCard(): boolean {

        if (!this.model.dateIn) {
            this.dialogService.alert("Date in is required");
            return false;
        }

        if (!this.model.dateOut) {
            this.dialogService.alert("Date out is required");
            return false;
        }

        if (this.commonService.isDateObjIsString(this.model.dateIn)) {
            this.model.dateIn = this.commonService.convertStringToDate(this.model.dateIn);
            this.model.dateOut = this.commonService.convertStringToDate(this.model.dateOut);
        }

        // Ignore seconds.
        this.model.dateIn.setSeconds(0);
        this.model.dateOut.setSeconds(0);

        if (this.model.dateOut.getTime() <= this.model.dateIn.getTime()) {
            this.dialogService.alert("Date out should be greater than Date In");

            return false;
        }

        //if (!this.model.odometer) {
        //    this.dialogService.alert("Odometer is required");
        //    return false;
        //}

        if (this.model.hourmeter < 1) {
            this.dialogService.alert("Hour meter should have a positive value");
            return false;
        }

        if (this.model.odometer < 1) {
            this.dialogService.alert("Odometer should have a positive value");
            return false;
        }

        if (this.currentVehicle.customerName !== "KCRM" &&
            this.fileUploader.uploadedFiles.length === 0) {

            this.dialogService.alert("Please select at least one file.");
            return false;
        }

        return true;
    }

    saveJobCard() {
        const isValid = this.validateBeforeSavingJobCard();

        if (!isValid) {
            return;
        }

        this.model.vehicleId = this.data.vehicleId;

        this.isActive = true;

        if (this.model.dateIn) {
            this.model.dateIn = this.commonService.getDateTimeAsString(this.model.dateIn);
        }

        if (this.model.dateOut) {
            this.model.dateOut = this.commonService.getDateTimeAsString(this.model.dateOut);
        }

        if (this.fileUploader !== undefined) {
            this.model.files = this.fileUploader.uploadedFiles;
        }

        this.jobCardService.addJobCard(this.model).subscribe(
            jobCardRes => {

                // Upload files after saving the job card.
                this.uploadFilesAfterSavingData(jobCardRes);
                this.jobCardAfterSaving(jobCardRes);
            });
    }

    uploadFilesAfterSavingData(responseResult) {
        if (responseResult["isSucceeded"] === true) {

            const filesCount: number = responseResult.data.fileList.filePaths.length;

            if (filesCount === 0) {
                return;
            }

            const filePaths: string[] = [];

            for (let i = 0; i < filesCount; i++) {

                filePaths.push(responseResult.data.fileList.filePaths[i]);
            }

            this.fileUploader.uploadFiles(filePaths);
        }
    }

    jobCardAfterSaving(jobCardRes: any) {
        if (jobCardRes["isSucceeded"] === true) {
            this.visibleAnimate = false;
            this.isActive = false;
            setTimeout(() => (this.visible = false), 300);

            this.dialogService.alert("Saving job card succeeded.");

            this.dialogRef.close();
        } else {

            var message = "Saving job card failed. ";

            if (jobCardRes.statusCode === 0) {
                console.log(jobCardRes.message);
            } else {
                message += jobCardRes.message;
            }

            this.dialogService.alert(message);
        }
    }
}
