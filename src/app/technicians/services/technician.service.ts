import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { Technician } from "../models/technician.model";

@Injectable()
export class TechnicianService {
  constructor(private http: HttpClient, private config: WebApiService) { }

  getTechnicians(): Observable<any> {
    const url = this.config.baseUrl + "Technician/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <Technician[]>data;
      return result;
    }));
  }

  addTechnician(model): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Technician/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  updateTechnician(model): Observable<any> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Technician/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  deleteTechnician(id) {
    let url = "{0}Technician/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  getTechnicianWithSearch(keyword): Observable<any> {
    const urlStatic = "Technician/SearchByWord?keyword={0}";
    const url = this.config.baseUrl + urlStatic.replace("{0}", keyword);

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }
}
