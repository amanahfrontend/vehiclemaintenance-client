import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";

import { DialogService } from "../../shared/services/dialog.service";
import { TechnicianAddComponent } from "../add/technician-add.component";
import { TechnicianService } from "../services/technician.service";

@Component({
    selector: "app-technicians-list",
    templateUrl: "./technicians-list.component.html",
    styleUrls: ["./technicians-list.component.scss"],
    providers: [TechnicianService]
})
export class TechniciansListComponent implements OnInit {
    technicians: any = [];
    isActive: boolean = false;
    query: string = "";

    constructor(
        private technicianService: TechnicianService,
        private customDialogService: DialogService,
        public dialog: MatDialog) {

    }

    ngOnInit() {
        this.getTechnicians();
    }

    openAddTec(): void {

        const dialogOptions = {
            width: "550px",
            data: { id: 0 }
        };

        let dialogRef = this.dialog.open(TechnicianAddComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {

            this.getTechnicians();
        });
    }

    getTechnicians() {
        this.isActive = true;

        this.technicianService.getTechnicians().subscribe(
            technicianRes => {

                if (technicianRes["isSucceeded"] === true) {
                    this.technicians = technicianRes["data"];
                    this.isActive = false;
                }

            });
    }

    deleteTechnician(id) {

        const question = "Do you really want to delete This technician?";
        const result = this.customDialogService.confirm("Confirm Delete", question, true);

        result.subscribe(
            (res) => {
                if (res) {
                    this.technicianService.deleteTechnician(id).subscribe(
                        technicianRes => {

                            if (technicianRes["isSucceeded"] === true) {

                                this.getTechnicians();
                            }
                        });
                }
            },
            () => {
                console.log("declined");
            }
        );
    }

    getTechnicianWithSearch() {
        this.isActive = true;

        this.technicianService.getTechnicianWithSearch(this.query).subscribe(
            result => {
                this.technicians = result["data"];
                this.isActive = false;
            },
            () => {
                console.log("error");
            });
    }

    searchTechnicians() {
        if (this.query) {
            this.getTechnicianWithSearch();
        }
        else {
            this.getTechnicians();
        }
    }
}
