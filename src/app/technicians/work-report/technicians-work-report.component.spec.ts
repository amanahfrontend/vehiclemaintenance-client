import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TechniciansWorkReportComponent } from "./technicians-work-report.component";

describe("TechniciansWorkReportComponent", () => {
    let component: TechniciansWorkReportComponent;
    let fixture: ComponentFixture<TechniciansWorkReportComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TechniciansWorkReportComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TechniciansWorkReportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
