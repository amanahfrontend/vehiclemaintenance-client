import { Component, OnInit } from "@angular/core";
import { RepairRequestSparePartService } from "../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { CommonService } from "../../shared/services/common.service";
import { TechnicianWorkSearchParams } from "../models/technician-work-report.model";
import { Technician } from "../models/technician.model";
import { TechnicianService } from "../services/technician.service";

@Component({
    selector: "app-technicians-work-report",
    templateUrl: "./technicians-work-report.component.html",
    providers: [TechnicianService, RepairRequestSparePartService, CommonService]
})
export class TechniciansWorkReportComponent implements OnInit {
    // Search criteria for the component.
    searchParams: TechnicianWorkSearchParams = new TechnicianWorkSearchParams();

    arrTechniciansWorkReport: any[] = [];
    technicians: Technician[];
    showLoading = false;

    constructor(
        private readonly technicianService: TechnicianService,
        private readonly repairRequestSparePartService: RepairRequestSparePartService,
        private readonly commonService: CommonService) {

    }

    ngOnInit() {
        this.getTechnicians();
    }

    searchTechniciansWork() {
        this.showLoading = true;

        this.searchParams.startDate = this.commonService.getDateAsString(this.searchParams.startDate);
        this.searchParams.endDate = this.commonService.getDateAsString(this.searchParams.endDate);

        this.repairRequestSparePartService.getTechniciansWorkReport(this.searchParams).subscribe(
            responseResult => {
                if (responseResult && responseResult.isSucceeded) {
                    this.arrTechniciansWorkReport = responseResult.data;
                    this.showLoading = false;
                }
            });
    }

    getTechnicians() {
        this.showLoading = true;

        this.technicianService.getTechnicians().subscribe(
            responseResult => {
                if (responseResult && responseResult.isSucceeded) {
                    this.technicians = responseResult.data;
                    this.showLoading = false;
                }
            });
    }

    clearSearch() {
        this.searchParams.clearValues();
        this.arrTechniciansWorkReport = [];
    }

    printTechnicianWorkReport() {
        const startDate = this.commonService.getDateAsString(this.searchParams.startDate);
        const endDate = this.commonService.getDateAsString(this.searchParams.endDate);
        const technicianId = this.searchParams.technicianId;

        const queryStringParams: string = `startDate=${startDate}&endDate=${endDate}&technicianId=${technicianId}`;

        // 7 = TechniciansWorkReport.rdl
        this.commonService.print2(7, queryStringParams);
    }
}
