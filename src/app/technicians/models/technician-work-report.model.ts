export class TechnicianWorkReport {

}

export class TechnicianWorkSearchParams {
    technicianId: number;
    startDate: string;
    endDate: string;

    constructor() {
        this.clearValues();
    }

    clearValues() {
        this.technicianId = 0;
        this.startDate = "";
        this.endDate = "";
    }
}
