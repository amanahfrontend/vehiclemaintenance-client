export class Technician {
    id: number;
    firstName: string;
    lastName: string;
    fullName: string;
    address: string;
    phoneNumber: string;
    specialty: string;
    employeId: string;
    hourRating: number;
    available: boolean;
}