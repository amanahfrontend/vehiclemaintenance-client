import { Component, OnInit } from "@angular/core";

import { Technician } from "../models/technician.model";
import { TechnicianService } from "../services/technician.service";

@Component({
    selector: "app-technician-man-power",
    templateUrl: "./technician-man-power.component.html",
    providers: [TechnicianService]
})
export class TechnicianManpowerComponent implements OnInit {
    technicians: Technician[];
    isActive: boolean = false;
    userRole: string;
    query: string = "";

    constructor(private technicianService: TechnicianService) { }

    ngOnInit() {
        this.getTechnicians();
    }

    getTechnicians() {
        this.isActive = true;
        this.userRole = sessionStorage.getItem("userRole");

        this.technicianService.getTechnicians().subscribe(
            technicianRes => {
                if (technicianRes["isSucceeded"] === true) {
                    this.technicians = technicianRes["data"];
                    this.isActive = false;
                }
            });
    }

    isManager() {
        return this.userRole === "Manager";
    }

    getTechnicianWithSearch() {
        this.isActive = true;

        this.technicianService.getTechnicianWithSearch(this.query).subscribe(
            result => {
                this.technicians = result["data"];
                this.isActive = false;
            },
            () => {
                console.log("error");
            });
    }

    searchTechnicians() {

        if (this.query) {
            this.getTechnicianWithSearch();
        } else {
            this.getTechnicians();
        }
    }
}
