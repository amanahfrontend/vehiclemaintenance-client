import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../shared/services/web-api.service";

@Injectable()
export class DashboardService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getOpenJobCardsCount(): Observable<any> {
    const url = this.config.baseUrl + "JobCard/OpenJobCardsCount";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getOpenRepairRequestCount(): Observable<any> {
    const url = this.config.baseUrl + "RepairRequest/OpenRepairRequestsCount";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getUsersCount(): Observable<any> {
    const url = this.config.baseUrl + "user/Count";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getRepairRequestChart(): Observable<any> {
    const url = this.config.baseUrl + "RepairRequest/GetChart";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getJobCardChart(): Observable<any> {
    const url = this.config.baseUrl + "JobCard/GetChart";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }
}
