import { MdlDialogService } from "@angular-mdl/core";
import { CommonModule } from "@angular/common";
import { Component, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { DashboardRouting } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";

@NgModule({
    imports: [
        CommonModule, FormsModule, DashboardRouting
    ],

    declarations: [
        //DashboardComponent
    ],

    providers: [MdlDialogService]
})
export class DashboardModule {
}