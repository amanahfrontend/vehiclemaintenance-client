import { Component, OnInit } from "@angular/core";
import { Chart } from "angular-highcharts";
import * as Chartist from "chartist";
import { ChartType, LegendItem } from "../lbd/lbd-chart/lbd-chart.component";
import { DashboardService } from "./dashboard.service";
import { VehicleService } from "../vehicles/services/vehicle.service";
import { CommonService } from "../shared/services/common.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
  providers: [DashboardService, VehicleService, CommonService]
})
export class DashboardComponent implements OnInit {
  public dataRepairs: any;
  public optionsRepairs: any;
  public dataCards: any;
  public optionsCards: any;
  public distributeSeries: boolean = true;
  public emailChartType: ChartType;
  public emailChartData: any;
  public emailChartLegendItems: LegendItem[];

  public hoursChartType: ChartType;
  public hoursChartData: any;
  public hoursChartOptions: any;
  public hoursChartResponsive: any[];
  public hoursChartLegendItems: LegendItem[];

  constructor(
    private readonly dashboardS: DashboardService,
    private readonly vehicleService: VehicleService,
    private readonly commonService: CommonService) { }

  openJobCardsCount: number;
  openRepairRequestsCount: number;
  vehiclesCount: number;
  usersCount: number;
  requestChart: any = [];
  cardsChart: any = [];
  isActive: boolean = false;
  chartRequests: any = {};
  chartJobs: any = {};

  ngOnInit() {
    this.getOpenJobCardsCount();
    this.getOpenRepairRequestsCount();
    this.getVehiclesCount();
    this.getUsersCount();

    //this.getRepairChartHigh(); //{Osman: to be enabled}
    //this.getJobsChartHigh();//{Osman: to be enabled}

    // this.getRepairChart();
    // this.getJobCardChart();
    // this.getRepairChart2();

    this.dataRepairs = {
      labels: [],
      series: []
    };
    this.optionsRepairs = {
      width: 400,
      height: 300
    };
    this.dataCards = {
      labels: [],
      series: []
    };
    this.optionsCards = {
      width: 400,
      height: 300
    };

    this.emailChartType = ChartType.Pie;
    this.emailChartData = {
      labels: [55, 12, 60],
      series: [55, 12, 60]
    };

    this.emailChartLegendItems = [
      { title: "Open", imageClass: "fa fa-circle text-info" },
      { title: "Bounce", imageClass: "fa fa-circle text-danger" },
      { title: "Unsubscribe", imageClass: "fa fa-circle text-warning" }
    ];

    this.hoursChartType = ChartType.Line;
    this.hoursChartData = {
      labels: [
        "9:00AM",
        "12:00AM",
        "3:00PM",
        "6:00PM",
        "9:00PM",
        "12:00PM",
        "3:00AM",
        "6:00AM"
      ],
      series: [
        [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
        [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
        [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
      ]
    };
    this.hoursChartOptions = {
      low: 0,
      high: 800,
      showArea: true,
      height: "245px",
      axisX: {
        showGrid: false
      },
      lineSmooth: Chartist.Interpolation.simple({
        divisor: 3
      }),
      showLine: false,
      showPoint: false
    };
    this.hoursChartResponsive = [
      [
        "screen and (max-width: 640px)",
        {
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            }
          }
        }
      ]
    ];
    this.hoursChartLegendItems = [
      { title: "Open", imageClass: "fa fa-circle text-info" },
      { title: "Click", imageClass: "fa fa-circle text-danger" },
      { title: "Click Second Time", imageClass: "fa fa-circle text-warning" }
    ];
  }

  getOpenJobCardsCount() {
    this.isActive = true;

    this.dashboardS.getOpenJobCardsCount().subscribe(
      result => {
        this.openJobCardsCount = result["data"];
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  getOpenRepairRequestsCount() {
    this.isActive = true;

    this.dashboardS.getOpenRepairRequestCount().subscribe(
      result => {
        this.openRepairRequestsCount = result["data"];
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  getVehiclesCount() {
    this.isActive = true;
    this.vehicleService.getVehiclesCount().subscribe(
      result => {
        this.vehiclesCount = result["data"];
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  getUsersCount() {
    this.isActive = true;
    this.dashboardS.getUsersCount().subscribe(
      result => {
        this.usersCount = result["data"];
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  getRepairChart() {
    this.isActive = true;
    this.dashboardS.getRepairRequestChart().subscribe(
      result => {
        this.requestChart = result["data"];
        for (let i = 0; i < this.requestChart.length; i++) {
          if (this.requestChart[i].nameEN != null) {
            this.dataRepairs.labels.push(this.requestChart[i].count);
            this.dataRepairs.series.push(this.requestChart[i].count);
            new Chartist.Pie("#chart1", this.dataRepairs, this.optionsRepairs);
          }
        }
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  getJobCardChart() {
    this.isActive = true;
    this.dashboardS.getJobCardChart().subscribe(
      result => {
        this.cardsChart = result["data"];
        for (let i = 0; i < this.cardsChart.length; i++) {
          if (this.cardsChart[i].nameEN != null) {
            this.dataCards.labels.push(this.cardsChart[i].nameEN);
            this.dataCards.series.push(this.cardsChart[i].count);
            new Chartist.Bar("#chart2", this.dataCards, this.distributeSeries);
          }
        }
        this.isActive = false;
        // console.log(JSON.stringify(this.dataCards.labels));
        // console.log(JSON.stringify(this.dataCards.series));
      },
      error => {
        console.log(error);
      }
    );
  }

  getRepairChart2() {
    this.isActive = true;
    this.dashboardS.getRepairRequestChart().subscribe(
      result => {
        this.requestChart = result["data"];
        for (let i = 0; i < this.requestChart.length; i++) {
          if (this.requestChart[i].nameEN != null) {
            // this.emailChartLegendItems.push(this.requestChart[i].nameEN);
            this.emailChartData.labels.push(this.requestChart[i].count);
            this.emailChartData.series.push(this.requestChart[i].count);
          }
        }
        this.isActive = false;
        // console.log(JSON.stringify(this.emailChartData.labels));
        // console.log(JSON.stringify(this.emailChartLegendItems));
      },
      error => {
        console.log(error);
      }
    );
  }

  getRepairChartHigh() {
    this.isActive = true;
    this.dashboardS.getRepairRequestChart().subscribe(
      result => {
        this.requestChart = result["data"];
        this.chartRequests = new Chart({
          chart: {
            type: "pie"
          },
          plotOptions: {
            pie: {
              showInLegend: true
            }
          },
          title: {
            text: "Repair Request Statistics"
          },
          series: [
            //{
            //    data: []
            //}
          ]
        });
        for (let i = 0; i < this.requestChart.length; i++) {
          if (this.requestChart[i].nameEN != null) {
            this.chartRequests._options.series[0].data.push({
              name: this.requestChart[i].nameEN,
              y: this.requestChart[i].count
            });
          }
        }
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  getJobsChartHigh() {
    this.isActive = true;
    this.dashboardS.getJobCardChart().subscribe(
      result => {
        this.cardsChart = result["data"];
        this.chartJobs = new Chart({
          chart: {
            type: "bar"
          },
          plotOptions: {
            pie: {
              showInLegend: true
            }
          },
          title: {
            text: "Job Card Statistics"
          },
          xAxis: {
            categories: []
          },
          series: [
            //{
            //    data: []
            //}
          ]
        });
        for (let i = 0; i < this.cardsChart.length; i++) {
          if (this.cardsChart[i].nameEN != null) {
            this.chartJobs._options.xAxis.categories.push(
              this.cardsChart[i].nameEN
            );
            this.chartJobs._options.series[0].data.push(
              this.cardsChart[i].count
            );
            // console.log(JSON.stringify(this.chartJobs._options.series))
          }
        }
        this.isActive = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  get currentUserIsNotEmployee(): boolean {
    return this.commonService.currentUserIsNotEmployee;
  }
}
