import { MdlDialogService } from "@angular-mdl/core";
import { Component, Input, OnInit } from "@angular/core";
import { DialogService } from "../../shared/services/dialog.service";
import { SparePart } from "../../spare-parts/models/spare-part.model";
import { Technician } from "../../technicians/models/technician.model";
import { RepairRequestMaterialService } from "../services/repair-request-materials.service";
import { SparePartStatusService } from "../../spare-parts/services/spare-part-status.service";

@Component({
  selector: "app-repair-request-materials-add-edit",
  templateUrl: "./repair-request-materials-add-edit.component.html",
  providers: [RepairRequestMaterialService, SparePartStatusService]
})
export class RepairRequestMaterialsAddEditComponent implements OnInit {

  @Input() repairRequestId: number;
  @Input() allowEdit: boolean;
  @Input() technicians: Technician[];
  @Input() spareParts: SparePart[];

  sparePartStatuses: any[];
  currentUserRole: string;

  modelMaterial: any = {};
  repairRequestMaterials: any[];

  isCollapsedMaterials = false;

  editModeMaterial: boolean = false;
  lblAddEditRequestMaterialText: string = "Add Lubricants";
  btnAddUpdateRequestMaterialText: string = "Add Lubricants";

  constructor(
    private readonly repairRequestMaterialService: RepairRequestMaterialService,
    private readonly sparePartStatusService: SparePartStatusService,
    private readonly customDialogService: DialogService,
    private readonly dialogService: MdlDialogService) { }

  ngOnInit() {
    this.currentUserRole = sessionStorage.getItem("userRole");

    this.getSparePartStatuses();
  }

  saveRepairRequestMaterial() {

    const saveRepairRequestMaterial = this.modelMaterial.id > 0
      ? this.repairRequestMaterialService.updateRepairRequestMaterial(this.modelMaterial)
      : this.repairRequestMaterialService.addRepairRequestMaterial(this.modelMaterial);

    saveRepairRequestMaterial.subscribe(
      responseResult => {
        if (responseResult["isSucceeded"] === true) {

          if (!this.editModeMaterial) {
            // If add mode of Repair request material, clear the model.

            this.modelMaterial = {
              repairRequestId: this.repairRequestId
            };
          } else {
            this.dialogService.alert("Record updated successfully.");
          }

          this.getRepairRequestMaterials(this.repairRequestId);
        } else {
          this.customDialogService.alert("Fail", "Something wrong.", false);
          console.log(JSON.stringify(responseResult.message));
        }
      });
  }

  deleteRepairRequestMaterial(repairRequestMaterialId: number) {
    const question = "Are you sure you want to delete this material of the repair request?";
    const result = this.dialogService.confirm(question, "No", "Yes");

    // if you need both answers
    result.subscribe(() => {

      this.repairRequestMaterialService.deleteRepairRequestMaterial(repairRequestMaterialId).subscribe(
        responseResult => {

          if (responseResult["isSucceeded"] === true) {
            this.getRepairRequestMaterials(this.repairRequestId);
          } else {
            this.dialogService.alert("Deleting failed. " + responseResult.message);
          }
        });
    });
  }

  getRepairRequestMaterials(repairRequestId) {
    this.repairRequestMaterialService.getByRepairRequestId(repairRequestId).subscribe(
      responseResult => {
        if (responseResult && responseResult["isSucceeded"] === true) {
          this.repairRequestMaterials = responseResult["data"];
        }
      });
  }

  openAddModeRepairRequestMaterial(): void {
    this.modelMaterial = {};

    this.modelMaterial.repairRequestId = this.repairRequestId;

    this.lblAddEditRequestMaterialText = "Add lubricants";
    this.btnAddUpdateRequestMaterialText = "Add lubricants";
    this.isCollapsedMaterials = !this.isCollapsedMaterials;
    this.editModeMaterial = false;
  }

  openEditModeRepairRequestMaterial(repairRequestMaterial: any) {
    Object.assign(this.modelMaterial, repairRequestMaterial);

    this.lblAddEditRequestMaterialText = "Edit lubricants";
    this.btnAddUpdateRequestMaterialText = "Update lubricants";
    this.isCollapsedMaterials = !this.isCollapsedMaterials;
    this.editModeMaterial = true;
  }

  getSparePartStatuses() {
    // Get Spare Part Status
    this.sparePartStatusService.getSparePartStatus().subscribe(
      responseResult => {
        if (responseResult["isSucceeded"] === true) {
          this.sparePartStatuses = responseResult["data"];
        }
      });
  }

  currentUserIsManager() {
    return (this.currentUserRole === "Manager");
  }

  showEditButton() {
    return this.allowEdit && this.currentUserIsManager() && !this.isCollapsedMaterials;
  }

  showDeleteButton() {
    return this.showEditButton();
  }
}
