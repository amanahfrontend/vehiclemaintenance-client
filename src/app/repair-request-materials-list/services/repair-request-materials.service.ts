import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";

@Injectable()
export class RepairRequestMaterialService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getUsedMaterials(): Observable<any[]> {
    const url = this.config.baseUrl + "RepairRequestMaterial/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }

  addRepairRequestMaterial(model): Observable<any> {
    model.createdByUserId = this.config.currentUserId;
    model.rowStatusId = 1;

    const url = this.config.baseUrl + "RepairRequestMaterial/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateRepairRequestMaterial(model): Observable<any> {
    model.modifiedByUserId = this.config.currentUserId;
    model.rowStatusId = 1;

    const url = this.config.baseUrl + "RepairRequestMaterial/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteRepairRequestMaterial(repairRequestMaterialId: number) {
    let url = "{0}RepairRequestMaterial/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}",
      repairRequestMaterialId.toString()).replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  getByRepairRequestId(repairRequestId): Observable<any> {
    const url = this.config.baseUrl + "RepairRequestMaterial/GetByRepairRequestID/" + repairRequestId;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getByjobcardId(id): Observable<any> {
    const url = this.config.baseUrl + "RepairRequestMaterial/GetByJobCardID/" + id;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getByRepairRequestIDs(repairrequestIds): Observable<any[]> {
    const url = this.config.baseUrl + "RepairRequestMaterial/GetByRepairRequestIDs";

    return this.http.post(url, repairrequestIds).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }

  getRepaireRequestWithSearch(query): Observable<any[]> {
    const urlStatic = "RepairRequestMaterial/SearchBysparePartSerialNumber_cost?word={0}";
    const url = this.config.baseUrl + urlStatic.replace("{0}", query);

    return this.http.get(url).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }
}
