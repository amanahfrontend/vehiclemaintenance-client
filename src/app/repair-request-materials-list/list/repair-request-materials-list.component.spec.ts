import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestMaterialsListComponent } from './repair-request-materials-list.component';

describe('RepairRequestMaterialsListComponent', () => {
  let component: RepairRequestMaterialsListComponent;
  let fixture: ComponentFixture<RepairRequestMaterialsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestMaterialsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestMaterialsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
