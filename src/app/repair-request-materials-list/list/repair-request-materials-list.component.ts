import { Component, OnInit } from "@angular/core";
import { RepairRequestMaterialService } from "../services/repair-request-materials.service";

///////////////////////////////////////////
// This component is not used.
///////////////////////////////////////////
@Component({
  selector: "app-repair-request-materials-list",
  templateUrl: "./repair-request-materials-list.component.html",
  providers: [RepairRequestMaterialService]
})
export class RepairRequestMaterialsListComponent implements OnInit {

  currentUserRole: string;

  repairRequestMaterials: any[];
  
  constructor(
    private readonly repairRequestMaterialService: RepairRequestMaterialService) { }

  ngOnInit() {
    this.currentUserRole = sessionStorage.getItem("userRole");
  }

  getRepairRequestMaterials(repairRequestId) {
    this.repairRequestMaterialService.getByRepairRequestId(repairRequestId).subscribe(
      responseResult => {
        if (responseResult && responseResult["isSucceeded"] === true) {
          this.repairRequestMaterials = responseResult["data"];
        }
      });
  }

  currentUserIsManager() {
    return (this.currentUserRole === "Manager");
  }
}
