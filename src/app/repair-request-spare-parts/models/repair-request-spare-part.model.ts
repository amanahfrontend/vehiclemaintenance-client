export class RepairRequestSparePart {
  id: number;
  repairRequestId: number;
  technicianId: number;
  technicianStartDate: Date;
  technicianEndDate: Date;
  amount: number;
  rowStatusId: number;
  sparePartId: number;
  sparePartName: string;
  description: string;
  comments: string;
  source: string;
}
