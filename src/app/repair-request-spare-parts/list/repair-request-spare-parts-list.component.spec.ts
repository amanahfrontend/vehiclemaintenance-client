import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestSparePartsListComponent } from './repair-request-spare-parts-list.component';

describe('RepairRequestSparePartsListComponent', () => {
  let component: RepairRequestSparePartsListComponent;
  let fixture: ComponentFixture<RepairRequestSparePartsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestSparePartsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestSparePartsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
