import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { RepairRequestSparePartService } from '../services/repair-request-spare-part.service';
import { RepairRequestSparePart } from '../models/repair-request-spare-part.model';

@Component({
  selector: "app-repair-request-spare-parts-pending-details",
  templateUrl: "./repair-request-spare-parts-pending-details.component.html",
  providers: [RepairRequestSparePartService]
})
export class RepairRequestSparePartsPendingDetailsComponent implements OnInit {
  sparepartsrequest: RepairRequestSparePart[];
  Id: string = sessionStorage.getItem("id");
  id: number;
  model: any = {};
  router: Router;

  private sub: any;

  constructor(router: Router,
    private dialogService: MdlDialogService,
    private sparepartsrequestservice: RepairRequestSparePartService,
    private route: ActivatedRoute) {
    this.router = router;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params["id"];
    });

    this.sparepartsrequestservice.getById(this.id).subscribe(
      requestSparePartsRes => {
        if (requestSparePartsRes["isSucceeded"] === true) {
          this.sparepartsrequest = requestSparePartsRes["data"];
          this.model = requestSparePartsRes["data"];
          console.log(this.model);
        } else {
          console.log(requestSparePartsRes.message);
        }
      });
  }

  delete() {

    this.sparepartsrequestservice.deleteRepairRequestSparePart(this.id).subscribe(
      jobcardRes => {

        if (jobcardRes["isSucceeded"] === true) {
          this.router.navigateByUrl("/home/prejobcards");
        }
      });
  }

  approve() {

    this.model.rowStatusId = 1;

    this.sparepartsrequestservice.updateRepairRequestSparePart(this.model).subscribe(
      sparepartsrequestRes => {

        if (sparepartsrequestRes["isSucceeded"] === true) {
          let result = this.dialogService.alert("Approved Successfully");
          result.subscribe(() => console.log("success"));
          this.router.navigateByUrl("/home/pending-repair-requests-spare-parts");
        } else {
          this.dialogService.alert("some thing wrong");
        }
      });
  }
}
