import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { TechnicianWorkSearchParams } from "../../technicians/models/technician-work-report.model";

@Injectable()
export class RepairRequestSparePartService {
  constructor(private http: HttpClient, private config: WebApiService) {

  }

  getSparePartsRequests(): Observable<any> {
    const url = this.config.baseUrl + "SparePartsRequest/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  addRepairRequestSparePart(model): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestSparePart/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  //addApproveSparePartsRequest(model): Observable<any> {
  //    model.fK_Status_ID = 2;
  //    const url = this.config.baseUrl + "SparePartsRequest/Add";

  //    return this.http.post(url, model).map(
  //        data => {
  //            const result = <any>data;
  //            return result;
  //        },
  //        error => {
  //            console.log(JSON.stringify(error.json()));
  //        }
  //    );
  //}

  updateRepairRequestSparePart(model): Observable<any> {
    // Don't put model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestSparePart/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  deleteRepairRequestSparePart(repairRequestSparePartId) {

    const url = this.config.baseUrl +
      "RepairRequestSparePart/Delete/" +
      repairRequestSparePartId +
      "/" +
      this.config.currentUserId;

    return this.http.delete(url).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  getByRepairRequest(repairRequestId: number, rowStatusId: number): Observable<any> {
    let url = "{0}RepairRequestSparePart/GetByRepairRequest/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl)
      .replace("{1}", repairRequestId.toString())
      .replace("{2}", rowStatusId.toString());

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getById(id): Observable<any> {
    const url = this.config.baseUrl + "RepairRequestSparePart/GetRepairRequestSparePart/" + id;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getPendingRepairRequestSpareParts(): Observable<any> {
    const url = this.config.baseUrl + "RepairRequestSparePart/GetPendingRepairRequestSpareParts";

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  getRepairRequestSparePartsOfJobCard(jobCardId: number): Observable<any> {
    const url = this.config.baseUrl + "RepairRequestSparePart/GetSparePartsOfJobCard/" + jobCardId;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  searchJobCardsUnDeliveredQty(startDate: string, endDate: string, jobCardId: string): Observable<any> {
    let url = "{0}RepairRequestSparePart/GetUndeliveredSpareParts?startDate={1}&endDateOut={2}&jobCardId={3}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", startDate)
      .replace("{2}", endDate).replace("{3}", jobCardId);

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  //getSparePartReport(sparePartId, startDate, endDate, vehicleFleetNo): Observable<any> {
  //    const staticUrl = "RepairRequestSparePart/SearchRepairRequestSpareParts?sparePartId={0}&startDate={1}&endDate={2}&vehicleFleetNo={3}";

  //    const url = this.config.baseUrl +
  //        staticUrl.replace("{0}", sparePartId).replace("{1}", startDate).replace("{2}", endDate)
  //            .replace("{3}", vehicleFleetNo);

  //    return this.http.get(url).map(data => {
  //        const result = data;
  //        return result;
  //    });
  //}

  getSparePartReportByQueryString(queryStringParams: string): Observable<any> {
    const url = `${this.config.baseUrl}RepairRequestSparePart/SearchRepairRequestSpareParts?${queryStringParams}`;

    return this.http.get(url).pipe(map(data => {
      const result = data;
      return result;
    }));
  }

  getTechniciansWorkReport(searchParams: TechnicianWorkSearchParams) {
    let url = "{0}RepairRequestSparePart/GetTechniciansWorkReport?startDate={1}&endDateOut={2}&technicianId={3}";

    const startDate = searchParams.startDate;
    const endDate = searchParams.endDate;
    const technicianId = searchParams.technicianId;

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", startDate)
      .replace("{2}", endDate).replace("{3}", technicianId.toString());

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }
}
