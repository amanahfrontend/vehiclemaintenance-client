import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { RepairRequestSparePartService } from "../services/repair-request-spare-part.service";

@Component({
  selector: "app-repair-request-spare-parts-pending",
  templateUrl: "./repair-request-spare-parts-pending.component.html",
  styleUrls: ["./repair-request-spare-parts-pending.component.css"],
  providers: [RepairRequestSparePartService]
})
export class RepairRequestSparePartsPendingComponent implements OnInit {
  repairRequestSpareParts: any = [];
  type: boolean;
  isActive: boolean = false;

  constructor(
    private repairRequestSparePartService: RepairRequestSparePartService,
    private dialogService: MdlDialogService
  ) {
  }

  ngOnInit() {
    this.getRepairRequestSpareParts();
  }

  getRepairRequestSpareParts() {
    this.isActive = true;

    this.repairRequestSparePartService.getPendingRepairRequestSpareParts().subscribe(
      repairRequestSparePartsRes => {

        if (repairRequestSparePartsRes["isSucceeded"]) {
          this.repairRequestSpareParts = repairRequestSparePartsRes["data"];
        }

        this.isActive = false;
      });
  }

  approve(model) {
    const question = "Are you sure you want to approve this spare part?";
    const result = this.dialogService.confirm(question, "Later", "Yes");

    // if you need both answers
    result.subscribe(
      () => {
        model.rowStatusId = 1;

        this.repairRequestSparePartService.updateRepairRequestSparePart(model).subscribe(
          sparepartsrequestRes => {
            if (sparepartsrequestRes["isSucceeded"] === true) {
              //const result = this.dialogService.alert("updated Successfully");
              //result.subscribe(() => this.getAllSpares());
              this.getRepairRequestSpareParts();
            } else {
              this.dialogService.alert("some thing wrong");
            }
          });
      },
      () => {
        console.log("declined");
      }
    );
  }
}
