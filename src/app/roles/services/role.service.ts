import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { Role } from "../models/role.model";

@Injectable()
export class RoleService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getRoles(): Observable<Role[]> {
    const url = this.config.baseUrl + "Role/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <Role[]>data;
      return result;
    }));
  }

  addRole(model): Observable<Role> {
    const url = this.config.baseUrl + "Role/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <Role>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error));
      }));
  }

  updateRole(model): Observable<Role> {
    const url = this.config.baseUrl + "Role/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <Role>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error));
      }));
  }

  deleteRole(roleName) {
    const url = this.config.baseUrl + "Role/Delete/" + roleName;

    return this.http.delete(url).pipe(map(
      data => {
        const result = <Role>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error));
      }));
  }
}
