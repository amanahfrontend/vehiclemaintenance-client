import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";

import { DialogService } from "../../shared/services/dialog.service";
import { RoleAddComponent } from "../add/role-add.component";
import { RoleService } from "../services/role.service";

@Component({
    selector: "app-roles-list",
    templateUrl: "./roles-list.component.html",
    styleUrls: ["./roles-list.component.css"],
    providers: [RoleService]
})
export class RolesListComponent implements OnInit {
    roles: any[] = [];
    isActive: boolean = false;

    constructor(private roleService: RoleService,
        private customDialogService: DialogService,
        private dialog: MatDialog) {
    }

    ngOnInit() {
        this.getAllRoles();
    }

    getAllRoles() {
        this.isActive = true;

        this.roleService.getRoles().subscribe(
            roleRes => {
                if (roleRes["isSucceeded"] === true) {
                    this.roles = roleRes["data"];
                    this.isActive = false;
                }
            });
    }

    openAddRole(): void {

        const dialogOptions = {
            width: "450px",
            data: { id: 0 }
        };

        let dialogRef = this.dialog.open(RoleAddComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getAllRoles();
        });
    }

    deleteRole(roleName) {
        const question = "Do you really want to delete this role?";
        const result = this.customDialogService.confirm("Confirm", question, true);

        result.subscribe(
            (res) => {
                if (res) {

                    this.roleService.deleteRole(roleName).subscribe(
                        roleRes => {

                            if (roleRes["isSucceeded"] === true) {
                                this.getAllRoles();
                            }

                        });
                }
            },
            () => {
                console.log("declined");
            }
        );
    }
}
