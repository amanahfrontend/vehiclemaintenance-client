﻿import { BaseEntity } from "../../shared/models/base-entity.model";

export class SparePartDeliveryOrder extends BaseEntity {
    requestedBy: string;
    sparePartDeliveryOrderDetails: SparePartDeliveryOrderDetails[];
}

export class SparePartDeliveryOrderDetails extends BaseEntity {
    repairRequestSparePartId: number;
    sparePartId: number;
    deliveredQty: number;
}

export class DeliveryOrderSearchParams {
    startDate?: string;
    endDate?: string;

    deliveryOrderId?: string;

    constructor() {
        this.clearValues();
    }

    clearValues() {
        this.startDate = "";
        this.endDate = "";
        this.deliveryOrderId = "";
    }
}