import { TestBed, inject } from "@angular/core/testing";

import { SparePartsDeliveryOrdersService } from "./spare-parts-delivery-orders.service";

describe("SparePartsDeliveryOrdersService", () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SparePartsDeliveryOrdersService]
        });
    });

    it("should be created", inject([SparePartsDeliveryOrdersService], (service: SparePartsDeliveryOrdersService) => {
        expect(service).toBeTruthy();
    }));
});
