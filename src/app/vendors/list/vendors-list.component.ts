import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { DialogService } from "../../shared/services/dialog.service";
import { VendorsService } from "../services/vendors.service";
import { VendorAddEditComponent } from "../add-edit/vendor-add-edit.component";

@Component({
    selector: "app-vendors-list",
    templateUrl: "./vendors-list.component.html",
    providers: [VendorsService]
})
export class VendorsListComponent implements OnInit {
    showLoading: boolean = false;
    vendors: any = [];

    constructor(
        private vendorsService: VendorsService,
        private customDialogService: DialogService,
        private dialog: MatDialog) { }

    ngOnInit() {
        this.getVendors();
    }

    getVendors() {

        // get Vendors
        this.vendorsService.getVendors().subscribe(vendorRes => {
            if (vendorRes["isSucceeded"] === true) {
                this.vendors = vendorRes["data"];
            }
        });
    }

    // In case of add, pass id.
    // In case of edit, pass id and model.
    openAddEditDialog(id: number, model: any) {

        const dialogOptions = {
            width: "500px",
            data: { id: id, model: model }
        };

        let dialogRef = this.dialog.open(VendorAddEditComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getVendors();
        });

    }

    deleteVendor(id) {
        const question = "Do you really want to delete this record?";
        let result = this.customDialogService.confirm("Confirm", question, true);

        result.subscribe(
            (result) => {
                if (result) {

                    this.vendorsService.deleteVendor(id).subscribe(
                        vendorResponse => {

                            if (vendorResponse["isSucceeded"] === true) {

                                this.getVendors();
                            }
                        });
                }
            },
            () => {
                console.log("Declined");
            }
        );
    }
}
