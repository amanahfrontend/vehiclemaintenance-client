import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { IVendor } from "../models/IVendor.interface";

@Injectable()
export class VendorsService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getVendors(): Observable<any[]> {
    const url = this.config.baseUrl + "Vendor/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }

  addVendor(model): Observable<IVendor> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Vendor/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateVendor(model): Observable<IVendor> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Vendor/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteVendor(id) {

    let url = "{0}Vendor/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  vendorReport(id): Observable<any> {
    const url = this.config.baseUrl + "Vendor/Get/" + id;
    return this.http.get(url).pipe(map(response => response));
  }
}
