import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Observable } from "rxjs";
import { DialogService } from "../../shared/services/dialog.service";
import { IVendor } from "../models/IVendor.interface";
import { VendorsService } from "../services/vendors.service";

@Component({
    selector: "app-vendor-add-edit",
    templateUrl: "./vendor-add-edit.component.html",
    providers: [VendorsService]
})
export class VendorAddEditComponent implements OnInit {
    title: string = "";
    id: number = 0;
    model: any = {};

    constructor(
        private vendorsService: VendorsService,
        private customDialogService: DialogService,
        private dialogRef: MatDialogRef<VendorAddEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
        this.id = this.data.id;

        if (this.id !== 0) {
            Object.assign(this.model, this.data.model);

            this.title = "Edit Vendor";
        } else {
            this.title = "Add Vendor";
        }
    }

    closeButtonClick(): void {
        this.dialogRef.close();
    }

    saveVendor() {
        let requestSaveVendor: Observable<IVendor>;

        if (this.id === 0) {
            requestSaveVendor = this.vendorsService.addVendor(this.model);
        } else {
            requestSaveVendor = this.vendorsService.updateVendor(this.model);
        }

        requestSaveVendor.subscribe(
            vendorResponse => {
                if (vendorResponse["isSucceeded"] === true) {
                    this.dialogRef.close();
                }
                else {
                    const errorMessage = this.formulateErrorMessage(vendorResponse);
                    this.customDialogService.alert("Fail", errorMessage, false);
                }
            });
    }


    private formulateErrorMessage(responseResult) {
        let errorMessage = "Failed to save the record. ";

        if (responseResult.statusCode !== 0) {
            errorMessage += responseResult.message;
        } else {
            console.log(responseResult.message);
        }

        return errorMessage;
    }
}
