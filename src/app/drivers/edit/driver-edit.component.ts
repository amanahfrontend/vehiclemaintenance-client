import { Component, EventEmitter, OnInit, Output } from "@angular/core";

import { DialogService } from "../../shared/services/dialog.service";
import { DriverService } from "../services/driver.service";

@Component({
  selector: "app-driver-edit",
  templateUrl: "./driver-edit.component.html",
  styleUrls: ["./driver-edit.component.css"],
  providers: [DriverService, DialogService]
})
export class DriverEditComponent implements OnInit {
  public visible = false;
  public visibleAnimate = false;
  model: any = {};
  licenseIssuedGovernates = [];

  @Output() dialogClosed = new EventEmitter();

  constructor(
    private driverService: DriverService,
    private customDialogService: DialogService) {
  }

  ngOnInit() {
    this.getLicenseIssuedGovernates();
  }

  private getLicenseIssuedGovernates() {

    this.licenseIssuedGovernates = this.driverService.getLicenseIssuedGovernates();
  }

  show(): void {
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  showDialog(model): void {

    Object.assign(this.model, model);

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  private validateBeforeSavingDriver(): boolean {
    if (!this.model.firstName) {
      this.customDialogService.alert("", "Driver first name is required", false);
      return false;
    }

    if (!this.model.lastName) {
      this.customDialogService.alert("", "Driver last name is required", false);
      return false;
    }

    if (!this.model.phoneNumber) {
      this.customDialogService.alert("", "Phone number is required", false);
      return false;
    }

    if (!this.model.address) {
      this.customDialogService.alert("", "Address is required", false);
      return false;
    }

    if (!this.model.title) {
      this.customDialogService.alert("", "Title is required", false);
      return false;
    }

    if (!this.model.employeeId) {
      this.customDialogService.alert("", "Employee Id is required", false);
      return false;
    }

    if (!this.model.ssn) {
      this.customDialogService.alert("", "SSN is required", false);
      return false;
    }

    if (!this.model.birthDate) {
      this.customDialogService.alert("", "Birth date is required", false);
      return false;
    }

    if (!this.model.licenseIssuedDate) {
      this.customDialogService.alert("", "License issued date is required", false);
      return false;
    }

    if (!this.model.licenseIssuedState) {
      this.customDialogService.alert("", "License issued state is required", false);
      return false;
    }

    if (!this.model.licenseNumber) {
      this.customDialogService.alert("", "License number is required", false);
      return false;
    }

    if (!this.model.civilIdNo) {
      this.customDialogService.alert("", "Civil Id No is required", false);
      return false;
    }

    return true;
  }

  updateDriver() {
    const isValid = this.validateBeforeSavingDriver();

    if (!isValid) {
      return;
    }

    this.model.rowStatusId = this.model.active ? 1 : 0;

    this.driverService.updateDriver(this.model).subscribe(
      driverRes => {

        if (driverRes["isSucceeded"] === true) {
          this.visibleAnimate = false;
          setTimeout(() => this.visible = false, 300);
          //this.customDialogService.alert("Done", "updated successfully", false);

          this.dialogClosed.emit();
        } else {
          this.customDialogService.alert("Fail", "some thing wrong", false);
        }

      });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
