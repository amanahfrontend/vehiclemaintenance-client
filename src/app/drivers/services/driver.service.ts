import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";

@Injectable()
export class DriverService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getDrivers(): Observable<any[]> {
    const url = this.config.baseUrl + "Driver/Get";

    return this.http.get(url).pipe(map(data => {
      let result = <any[]>data;
      return result;
    }));
  }

  addDriver(model): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Driver/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateDriver(model): Observable<any> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Driver/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteDriver(id) {
    let url = "{0}Driver/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        let result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  getDriversWithSearch(keyword): Observable<any[]> {
    const urlStatic = "Driver/SearchByWord?keyword={0}";
    const url = this.config.baseUrl + urlStatic.replace("{0}", keyword);

    return this.http.get(url).pipe(map(data => {
      let result = <any[]>data;
      return result;
    }));
  }

  public getLicenseIssuedGovernates() {
    const list = [];

    list.push({ id: 1, text: "Kuiwait City" });
    list.push({ id: 2, text: "Farwania" });
    list.push({ id: 3, text: "Hawali" });
    list.push({ id: 4, text: "Jahara" });
    list.push({ id: 5, text: "Ahmadi" });
    list.push({ id: 6, text: "Mubarak Al Kabir" });

    return list;
  }
}
