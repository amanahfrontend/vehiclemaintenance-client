import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { DialogService } from "../../shared/services/dialog.service";
import { DriverService } from "../services/driver.service";

@Component({
  selector: "app-driver-add",
  templateUrl: "./driver-add.component.html",
  providers: [DriverService]
})
export class DriverAddComponent implements OnInit {
  router: Router;
  visible = false;
  visibleAnimate = false;
  model: any = {};
  licenseIssuedGovernates = [];

  constructor(
    private driverService: DriverService,
    private customDialogService: DialogService,
    public dialogRef: MatDialogRef<DriverAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
    this.getLicenseIssuedGovernates();
  }

  private getLicenseIssuedGovernates() {

    this.licenseIssuedGovernates = this.driverService.getLicenseIssuedGovernates();
  }

  show(): void {
    this.model = {};
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  hide(): void {
    this.model = {};
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  saveDriver() {
    this.model.rowStatusId = this.model.active ? 1 : 0;

    this.driverService.addDriver(this.model).subscribe(
      driverRes => {
        if (driverRes["isSucceeded"] === true) {
          this.visibleAnimate = false;
          setTimeout(() => this.visible = false, 300);
          this.customDialogService.alert("Done", "added successfully", false);
          this.dialogRef.close();
        } else {
          this.dialogRef.close();
          this.customDialogService.alert("Fail", "Something wrong", false);
        }
      });
  }

  cancel() {
    this.model = {};
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
