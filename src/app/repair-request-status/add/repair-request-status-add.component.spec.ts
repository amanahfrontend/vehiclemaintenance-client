import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestStatusAddComponent } from './repair-request-status-add.component';

describe('RepairRequestStatusAddComponent', () => {
  let component: RepairRequestStatusAddComponent;
  let fixture: ComponentFixture<RepairRequestStatusAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestStatusAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestStatusAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
