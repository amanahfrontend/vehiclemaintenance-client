import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestStatusEditComponent } from './repair-request-status-edit.component';

describe('RepairRequestStatusEditComponent', () => {
  let component: RepairRequestStatusEditComponent;
  let fixture: ComponentFixture<RepairRequestStatusEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestStatusEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestStatusEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
