import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { RepairRequestStatusService } from "../services/repair-request-status.service";
import { DialogService } from "../../shared/services/dialog.service";

@Component({
    selector: "app-repair-request-status-edit",
    templateUrl: "./repair-request-status-edit.component.html",
    styleUrls: ["./repair-request-status-edit.component.scss"],
    providers: [RepairRequestStatusService]
})
export class RepairRequestStatusEditComponent implements OnInit {
    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    @Output() dialogClosed = new EventEmitter();

    constructor(
        private repairRequestStatusService: RepairRequestStatusService,
        private customDialogService: DialogService) {
    }

    ngOnInit() {

    }

    show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    showDialog(model): void {

        Object.assign(this.model, model);

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    updateRepairRequestStatus() {

        this.repairRequestStatusService.updateRepairRequestStatus(this.model).subscribe(
            repairRequestStatusRes => {

                if (repairRequestStatusRes["isSucceeded"] === true) {
                    //this.customDialogService.alert("Done", "updated successfully", false);
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);

                    this.visible = false;
                    this.dialogClosed.emit();

                } else {
                    this.customDialogService.alert("Fail", "Something wrong", false);
                }
            });
    }
}

