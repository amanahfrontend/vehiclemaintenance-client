import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { IRepairRequestStatus } from "../models/IRepairRequestStatus.interface";
import { DialogService } from "../../shared/services/dialog.service";
import { RepairRequestStatusService } from "../services/repair-request-status.service";
import { EnumRepairRequestStatus } from "../models/repair-request-status.model";
import { RepairRequestStatusAddComponent } from "../add/repair-request-status-add.component";

@Component({
    selector: "app-repair-request-status-list",
    templateUrl: "./repair-request-status-list.component.html",
    styleUrls: ["./repair-request-status-list.component.css"],
    providers: [RepairRequestStatusService]
})
export class RepairRequestStatusListComponent implements OnInit {
    repairRequestStatuses: IRepairRequestStatus[];
    isActive: boolean = false;

    constructor(private repairrequeststatusService: RepairRequestStatusService,
        private customDialogService: DialogService,
        public dialog: MatDialog) {
    }

    ngOnInit() {
        this.getAllRepairStatus();
    }

    getAllRepairStatus() {
        this.isActive = true;

        this.repairrequeststatusService.getRepairRequestStatus().subscribe(
            repairrequeststatusRes => {

                if (repairrequeststatusRes["isSucceeded"] === true) {
                    this.repairRequestStatuses = repairrequeststatusRes["data"];
                    this.isActive = false;
                }

            });
    }

    openAddRepairStatus(): void {

        const dialogOptions = {
            width: "500px",
            data: { id: 0 }
        };

        let dialogRef = this.dialog.open(RepairRequestStatusAddComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(
            () => {

                this.getAllRepairStatus();
            });
    }

    deleteRepairRequestStatus(id) {

        let result = this.customDialogService.confirm("Delete status",
            "Do you really want to delete This status?",
            true);

        result.subscribe(
            (res) => {
                if (res) {
                    this.repairrequeststatusService.deleteRepairRequestStatus(id).subscribe(
                        repairrequesttypeRes => {

                            if (repairrequesttypeRes["isSucceeded"] === true) {

                                this.getAllRepairStatus();
                            }
                        });
                }
            },
            () => {
                console.log("declined");
            }
        );
    }

    showEditDeleteLinks(item: any): boolean {
        return item.id !== EnumRepairRequestStatus.NeedApproval
            && item.id !== EnumRepairRequestStatus.UnderQuotation
            && item.id !== EnumRepairRequestStatus.Closed
            && item.id !== EnumRepairRequestStatus.InProgress;
    }
}
