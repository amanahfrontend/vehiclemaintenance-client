import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestStatusListComponent } from './repair-request-status-list.component';

describe('RepairRequestStatusComponent', () => {
    let component: RepairRequestStatusListComponent;
    let fixture: ComponentFixture<RepairRequestStatusListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RepairRequestStatusListComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RepairRequestStatusListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
