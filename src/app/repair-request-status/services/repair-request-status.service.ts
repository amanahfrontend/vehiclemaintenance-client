import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { IRepairRequestStatus } from "../models/IRepairRequestStatus.interface";

@Injectable()
export class RepairRequestStatusService {

  constructor(private http: HttpClient, private config: WebApiService) {
  }

  getRepairRequestStatus(): Observable<IRepairRequestStatus[]> {
    const url = this.config.baseUrl + "RepairRequestStatus/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <IRepairRequestStatus[]>data;
      return result;
    }));
  }

  addRepairRequestStatus(model): Observable<IRepairRequestStatus> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestStatus/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <IRepairRequestStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateRepairRequestStatus(model): Observable<IRepairRequestStatus> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestStatus/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <IRepairRequestStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteRepairRequestStatus(id) {
    let url = "{0}RepairRequestStatus/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const result = <IRepairRequestStatus>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
