import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { DamageMemoListComponent } from "./damage-memo-list.component";

describe("DamageMemoListComponent", () => {
  let component: DamageMemoListComponent;
  let fixture: ComponentFixture<DamageMemoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DamageMemoListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageMemoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
