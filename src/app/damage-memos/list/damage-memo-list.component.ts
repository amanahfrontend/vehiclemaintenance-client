import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { CommonService } from "../../shared/services/common.service";
import { DamageMemoAddEditComponent } from "../add-edit/damage-memo-add-edit.component";
import { DamageMemoService } from "../services/damage-memo.service";

@Component({
  selector: "app-damage-memo-list",
  templateUrl: "./damage-memo-list.component.html",
  providers: [DamageMemoService, CommonService]
})
export class DamageMemoListComponent implements OnInit {
  damageMemos: any = [];
  showLoading: boolean = false;
  showNoDataFoundLabel: boolean = false;

  constructor(
    private readonly damageMemoService: DamageMemoService,
    private readonly commonService: CommonService,
    private readonly dialog: MatDialog) { }

  ngOnInit() {
  }

  searchDamageMemos() {
    this.showLoading = true;

    this.damageMemoService.getDamageMemos().subscribe(
      responseResult => {
        if (responseResult && responseResult["isSucceeded"] === true) {
          this.damageMemos = responseResult["data"];

          this.showLoading = false;
        }
      });
  }

  openAddDamageMemoDialog(): void {

    const dialogOptions = {
      width: "850px",
      height: "458px",
      data: { id: 0 }
    };

    const dialogRef = this.dialog.open(DamageMemoAddEditComponent, dialogOptions);

    dialogRef.afterClosed().subscribe(() => {
      this.searchDamageMemos();
    });
  }

  printDamageMemoReport(damageMemoId: number) {
    this.commonService.print2(11, `damageMemoId=${damageMemoId}`);
  }

  getEmployeeType(item) {
    if (item.technicianId != null) {
      return "Technician";
    }

    if (item.driverId != null) {
      return "Driver";
    }

    return "";
  }

  getEmployeeName(item) {

    if (item.technician != null) {
      return item.technician.fullName;
    }

    if (item.driver != null) {
      return item.driver.fullName;
    }

    return "";
  }
}
