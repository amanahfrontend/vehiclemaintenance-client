import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DriverService } from "../../drivers/services/driver.service";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { TechnicianService } from "../../technicians/services/technician.service";
import { DamageMemoService } from "../services/damage-memo.service";

@Component({
  selector: "app-damage-memo-add-edit",
  templateUrl: "./damage-memo-add-edit.component.html",
  providers: [DamageMemoService, TechnicianService, DriverService, DialogService, CommonService]
})
export class DamageMemoAddEditComponent implements OnInit {

  model: any = {};
  technicians: any[];
  drivers: any[];

  title: string = "";
  btnTitle: string = "";
  visible = false;
  visibleAnimate = false;

  constructor(
    private readonly damageMemoService: DamageMemoService,
    private readonly technicianService: TechnicianService,
    private readonly driverService: DriverService,
    private dialogRef: MatDialogRef<DamageMemoAddEditComponent>,
    private readonly customDialogService: DialogService,
    private readonly commonService: CommonService,
    @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {
    this.setComponentLabels();
    this.getTechnicians();
    this.getDrivers();

    this.initCurrentModel();
  }

  initCurrentModel() {
    this.model.technicianId = 0;
    this.model.driverId = 0;
  }

  ddlTechniciansChanged() {
    this.model.driverId = 0;
  }

  ddlDriversChanged() {
    this.model.technicianId = 0;
  }

  private setComponentLabels() {

    if (this.data.id) {
      this.title = "Edit Damage Memo";
      this.btnTitle = "Update";
    } else {
      this.title = "Add Damage Memo";
      this.btnTitle = "Add";
    }
  }

  saveDamageMemo() {
    const isValid = this.validateBeforeSavingDamageMemo();

    if (!isValid) {
      return;
    }

    if (this.model.id) {

      this.damageMemoService.updateDamageMemo(this.model).subscribe(
        vehicleRes => this.showMessageAfterSave(vehicleRes));
    } else {

      this.damageMemoService.addDamageMemo(this.model).subscribe(
        vehicleRes => this.showMessageAfterSave(vehicleRes));
    }
  }

  validateBeforeSavingDamageMemo() {

    if (this.model.technicianId === 0 && this.model.driverId === 0) {
      this.customDialogService.alert("Required field", "Please select the employee (technician or driver).", false);
      return false;
    }

    if (this.model.technicianId === 0) {
      this.model.technicianId = null;
    }

    if (this.model.driverId === 0) {
      this.model.driverId = null;
    }

    return true;
  }

  closeButtonClick(): void {
    this.dialogRef.close();
  }

  private showMessageAfterSave(responseResult): void {

    if (responseResult["isSucceeded"] === true) {
      //this.customDialogService.alert("Done", operation + " successfully", false);
      this.visibleAnimate = false;
      setTimeout(() => (this.visible = false), 300);

      this.printDamageMemoReport(responseResult.data.id);

      this.dialogRef.close();
      this.model = {};


    } else {

      const errorMessage = this.formulateErrorMessage(responseResult);
      this.customDialogService.alert("Fail", errorMessage, false);
    }
  }

  private formulateErrorMessage(responseResult) {
    let errorMessage = "Failed to save the record. ";

    if (responseResult.statusCode !== 0) {
      errorMessage += responseResult.message;
    } else {
      console.log(responseResult.message);
    }

    return errorMessage;
  }

  printDamageMemoReport(damageMemoId: number) {
    this.commonService.print2(11, `damageMemoId=${damageMemoId}`);
  }

  getTechnicians() {

    // Get Technicians
    this.technicianService.getTechnicians().subscribe(
      technicianRes => {
        if (technicianRes["isSucceeded"] === true) {
          this.technicians = technicianRes["data"];
        }
      });
  }

  getDrivers() {
    this.driverService.getDrivers().subscribe(
      driverRes => {

        if (driverRes["isSucceeded"] === true) {
          this.drivers = driverRes["data"];
        }
      });
  }
}
