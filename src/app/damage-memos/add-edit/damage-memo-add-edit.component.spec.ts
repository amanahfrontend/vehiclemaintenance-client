import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageMemoAddEditComponent } from './damage-memo-add-edit.component';

describe('DamageMemoAddEditComponent', () => {
  let component: DamageMemoAddEditComponent;
  let fixture: ComponentFixture<DamageMemoAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageMemoAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageMemoAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
