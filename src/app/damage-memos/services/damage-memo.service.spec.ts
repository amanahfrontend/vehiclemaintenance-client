import { TestBed } from "@angular/core/testing";

import { DamageMemoService } from "./damage-memo.service";

describe("DamageMemoService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: DamageMemoService = TestBed.get(DamageMemoService);
    expect(service).toBeTruthy();
  });
});
