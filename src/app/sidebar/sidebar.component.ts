import { Component, OnInit } from "@angular/core";

interface IRouteInfo {
  path: string;
  title: string;
  icon?: string;
  cssClass?: string;
}

export const routes: IRouteInfo[] = [
  { path: "dashboard", title: "Dashboard", icon: "pe-7s-graph", cssClass: "" },
  { path: "user", title: "User Profile", icon: "pe-7s-user", cssClass: "" },
  {
    path: "settings",
    title: "Settings",
    icon: "pe-7s-note2",
    cssClass: "toggle accordion-toggle"
  },
  {
    path: "scheduledmaintenance",
    title: "Scheduled Maintenance",
    icon: "pe-7s-note2",
    cssClass: ""
  },
  //{
  //    path: "preventivemaintenance",
  //    title: "Preventive Maintenance",
  //    icon: "pe-7s-note2",
  //    cssClass: ""
  //},
  {
    path: "pending-repair-requests-spare-parts",
    title: "Pending repair requests spare parts",
    icon: "pe-7s-user",
    cssClass: ""
  },
  {
    path: "technician_manpower",
    title: "ManPower",
    icon: "pe-7s-power",
    cssClass: ""
  }
];

export const settingsRoutes: IRouteInfo[] = [
  { path: "settings/users/list", title: "Users" },
  { path: "settings/technicians/list", title: "Technicians" },
  { path: "settings/drivers/list", title: "Drivers" },
  { path: "settings/service-types/list", title: "Service Types" },
  { path: "settings/spare-parts/list", title: "Spare parts" },
  { path: "settings/vehicle-types/list", title: "Vehicle types" },
  { path: "settings/repair-request-types/list", title: "Repair Request Types" },
  { path: "settings/repair-request-statuses/list", title: "Repair Request Status" },
  { path: "settings/vendors/list", title: "Vendors" },
  { path: "settings/roles/list", title: "Roles" }
];

export const reportsRoutes: IRouteInfo[] = [
  { path: "job-cards/report", title: "Job Cards Report" },
  { path: "repair-requests/spare-parts/report", title: "Spare Parts Usage Report" },
  { path: "daily-services/report", title: "Daily Service Report" },
  { path: "technicians/report", title: "Technicians Work Report" },
  { path: "cannibalization/list", title: "Cannibalization Report" },
  { path: "damage-memo/list", title: "Damage Memo Report" }
  //{
  //    path: "maintenanceReport",
  //    title: "Preventive Maintenance",
  //    icon: "",
  //    cssClass: ""
  //},
  //{ path: "ospOrdersReport", title: "OSP Work Order", icon: "", class: "" },
  //{ path: "vendorsReport", title: "Vendors Report" },
  //{
  //    path: "toolsInspectionReport",
  //    title: "Tools Inspection",
  //    icon: "",
  //    cssClass: ""
  //},
  //{ path: "EquipmentReport", title: "Equipment Transfer", icon: "", class: "" },

  //{
  //    path: "dailyGarageReport",
  //    title: "Daily Work of Garage",
  //    icon: "",
  //    cssClass: ""
  //},
  //{
  //    path: "material-requisition-report",
  //    title: "Material Requisition Report",
  //    icon: "",
  //    cssClass: ""
  //},
  //{
  //    path: "internal-purchase-request-report",
  //    title: "Internal Purchase Request",
  //    icon: "",
  //    cssClass: ""
  //}
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  settingsMenuItems: any[];
  reportsMenuItems: any[];
  userRole: string;
  userName = JSON.parse(localStorage.getItem("userName"));


  ngOnInit() {
    this.userRole = sessionStorage.getItem("userRole");
    this.menuItems = routes.filter(menuItem => menuItem);

    this.settingsMenuItems = settingsRoutes.filter(
      menuItem => menuItem
    );

    this.reportsMenuItems = reportsRoutes.filter(
      menuItem => menuItem
    );
  }

  showDashboardMenu() {
    return this.isManager() || this.isEmployee();
  }

  showVehiclesMenu() {
    return this.isManager() || this.isEmployee();
  }

  showRepairRequestsMenu() {
    return this.isManager() || this.isEmployee();
  }

  showApproveMenu() {
    return this.isManager();
  }

  showReportsMenu() {
    return this.isManager() || this.isEmployee();
  }

  showSettingsMenu() {
    return this.isAdmin();
  }

  showDeliverSparePartsMenu() {
    return this.isStoreKeeper();//isStoreKeeper
  }

  isManager() {
    return this.userRole === "Manager";
  }

  isAdmin() {
    return this.userRole === "Admin";
  }

  isEmployee() {
    return this.userRole === "Employee";
  }

  isStoreKeeper() {
    return this.userRole === "Store keeper";
  }
}
