﻿export class UploadedFile {
    fileName: string;
    fileType: string;
}