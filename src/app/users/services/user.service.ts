import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";

@Injectable()
export class UserService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getUsers(): Observable<any[]> {
    return this.searchUsers("");
  }

  // In case keyword is null or empty, all users will be got.
  searchUsers(keyword): Observable<any[]> {
    const staticUrl = "User/Search?keyword={0}";
    const url = this.config.baseUrl + staticUrl.replace("{0}", keyword);

    return this.http.get(url).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }

  getUserById(userId: string): Observable<any> {

    const url = this.config.baseUrl + "User/Get/" + userId;

    return this.http.get(url).pipe(map(data => {
      const result = <any>data;
      return result;
    }));
  }

  login(model): Observable<any> {
    const url = this.config.baseUrl + "User/Login";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  addUser(model): Observable<any> {
    model.createdByUserId = this.config.currentUserId;
    const url = this.config.baseUrl + "User/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateUser(model): Observable<any> {
    model.modifiedByUserId = this.config.currentUserId;
    const url = this.config.baseUrl + "User/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateUserPassword(model): Observable<any> {
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "User/UpdateUserPassword";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteUser(username) {
    const url = this.config.baseUrl + "User/Delete/" + username;

    return this.http.delete(url).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
