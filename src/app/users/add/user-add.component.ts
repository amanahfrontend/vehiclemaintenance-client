import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { RoleService } from "../../roles/services/role.service";
import { DialogService } from "../../shared/services/dialog.service";
import { UserService } from "../services/user.service";

@Component({
  selector: "app-user-add",
  templateUrl: "./user-add.component.html",
  providers: [RoleService, UserService]
})
export class UserAddComponent implements OnInit {
  roles: any[] = [];
  router: Router;
  visible = false;
  visibleAnimate = false;
  model: any = {};
  role: string = "";
  users: any = [];
  isSubmitted: boolean = false;

  constructor(router: Router, private userservice: UserService,
    private roleservice: RoleService, private customDialogService: DialogService,
    public dialogRef: MatDialogRef<UserAddComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.router = router;
  }

  ngOnInit() {
    this.getRoles();
  }

  getRoles() {

    this.roleservice.getRoles().subscribe(
      roleRes => {

        if (roleRes["isSucceeded"] === true) {
          this.roles = roleRes["data"];
        }
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  show(): void {
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  getAllUsers() {
    this.userservice.getUsers().subscribe(
      userRes => {
        this.users = userRes;
      },
      () => {
        console.error();
      });
  }

  saveUser() {
    this.isSubmitted = true;

    this.userservice.addUser(this.model).subscribe(
      userRes => {
        if (userRes["isSucceeded"] === true) {
          this.visibleAnimate = false;
          this.getAllUsers();

          setTimeout(() => this.visible = false, 300);

          this.dialogRef.close();
        }
        else {
          this.customDialogService.alert("Fail", userRes.message, false);
        }
      });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
