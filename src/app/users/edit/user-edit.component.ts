import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormControl } from "@angular/forms";
import { DialogService } from "../../shared/services/dialog.service";
import { RoleService } from "../../roles/services/role.service";
import { UserService } from "../services/user.service";

@Component({
  selector: "app-user-edit",
  templateUrl: "./user-edit.component.html",
  styleUrls: ["./user-edit.component.css"],
  providers: [RoleService, UserService]
})
export class UserEditComponent implements OnInit {
  roles: any[] = [];
  visible = false;
  visibleAnimate = false;
  model: any = {};

  @Output() dialogClosed = new EventEmitter();

  constructor(
    private userservice: UserService,
    private roleservice: RoleService,
    private customDialogService: DialogService) {
  }

  ngOnInit() {
    this.getRoles();
  }

  private getRoles() {

    this.roleservice.getRoles().subscribe(
      roleRes => {

        if (roleRes["isSucceeded"] === true) {
          this.roles = roleRes["data"];
        }
      });
  }

  show(): void {
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  showDialog(model): void {
    Object.assign(this.model, model);
    this.model.roleNames = model.roleNames[0];

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  updateUser() {
    let role = this.model.roleNames;
    this.model.roleNames = [];
    this.model.roleNames.push(role);

    this.userservice.updateUser(this.model).subscribe(
      userRes => {
        if (userRes["isSucceeded"] === true) {
          this.visibleAnimate = false;
          setTimeout(() => this.visible = false, 300);
          //this.customDialogService.alert("Done", "updated successfully", false);

          this.dialogClosed.emit();
        }
        else {
          this.customDialogService.alert("Fail", "something wrong", false);
        }
      });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
