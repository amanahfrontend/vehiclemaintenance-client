import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/user.service";

@Component({
  selector: "app-user-change-password",
  templateUrl: "./user-change-password.component.html"
})
export class UserChangePasswordComponent implements OnInit {
  visible = false;
  visibleAnimate = false;
  model: any = {};

  constructor(
    private userService: UserService) {
  }

  ngOnInit() {

  }

  hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

  show(model): void {
    Object.assign(this.model, model);

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  updateUserPassword() {

    this.userService.updateUserPassword(this.model).subscribe(
      userRes => {

        if (userRes["isSucceeded"] === true) {
          this.visibleAnimate = false;
          setTimeout(() => this.visible = false, 300);
        }
      });
  }
}
