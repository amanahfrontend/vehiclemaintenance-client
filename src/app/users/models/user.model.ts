export class User {
    id?: string;
    userName?: string;

    firstName?: string;
    lastName?: string;
    phoneNumber?: string;

    email?: string;
    password?: string;

    birthDate?: Date;
    roleNames?: string[];
}
