import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";
import { Login } from "./login.model";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  router: Router;
  model = new Login();
  showLoading = false;

  constructor(
    router: Router,
    private userService: UserService,
    private mdlDialogService: MdlDialogService
  ) {
    this.router = router;
  }

  ngOnInit() { }

  login() {
    this.showLoading = true;

    return this.userService.login(this.model).subscribe(
      data => {
        localStorage.setItem("userName", JSON.stringify(data.userName));
        localStorage.setItem("userId", JSON.stringify(data.userId));

        var userRole = data.roles[0];
        var defaultUrl = userRole === "Manager" ? "home/dashboard" : "home";
        this.router.navigateByUrl(defaultUrl);

        sessionStorage.setItem("id", data.userId);
        sessionStorage.setItem("valid", "true");
        sessionStorage.setItem("userRole", userRole);

        this.showLoading = false;
      },
      error => {

        if (error.status === 500) {
          this.mdlDialogService.alert("Internal Server Error");
        }

        this.mdlDialogService.alert("Invalid user name or password.\n Please try again or contact the system administrator.");
        this.showLoading = false;
      }
    );
  }
}
