import { Component, OnInit } from "@angular/core";
import { JobCard } from "../job-cards/models/job-card.model";
import { JobCardService } from "../job-cards/services/job-card.service";


@Component({
    selector: "app-prejobcard",
    templateUrl: "./prejobcard.component.html",
    styleUrls: ["./prejobcard.component.css"],
    providers: [JobCardService]
})
export class preJobCardComponent implements OnInit {
    jobCards: JobCard[];
    type: boolean;
    isActive: boolean = false;

    constructor(private jobCardservice: JobCardService) { }

    ngOnInit() {
        this.isActive = true;

        this.jobCardservice.getJobCards().subscribe(
            jobCardRes => {
                if (jobCardRes["isSucceeded"] === true) {
                    this.jobCards = jobCardRes["data"];
                    this.isActive = false;
                }
                return this.jobCards;
            });
    }

}
