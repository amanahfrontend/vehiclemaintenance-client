﻿export class JobCardRepairRequestSearchCriteria {
    customerName: string;

    sparePartId?: number;
    serviceTypeId: number;

    startDate?: string;
    endDate?: string;

    vehiclePlateNo: string;
    vehicleFleetNo: string;

    jobCardId?: string;
    jobCardStatusId: number;

    constructor() {
        this.resetValues();
    }

    resetValues() {
        this.customerName = "0";

        this.sparePartId = 0;
        this.serviceTypeId = 0;

        this.startDate = "";
        this.endDate = "";

        this.vehiclePlateNo = "";
        this.vehicleFleetNo = "";

        this.jobCardId = "";
        this.jobCardStatusId = 0;
    }
}