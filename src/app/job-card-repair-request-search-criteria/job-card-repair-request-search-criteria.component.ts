import { Component, Input, OnInit, Output } from "@angular/core";
import { JobCardStatusService } from "../job-cards/services/job-card-status.service";
import { ServiceTypeService } from "../service-types/services/service-type.service";
import { CommonService } from "../shared/services/common.service";
import { SparePartService } from "../spare-parts/services/spare-part.service";
import { JobCardRepairRequestSearchCriteria } from "./job-card-repair-request-search-criteria.model";

@Component({
  selector: "app-job-card-repair-request-search-criteria",
  templateUrl: "./job-card-repair-request-search-criteria.component.html",
  providers: [SparePartService, ServiceTypeService, JobCardStatusService]
})
export class JobCardRepairRequestSearchCriteriaComponent implements OnInit {
  @Input() showCustomerName: boolean = true;
  @Input() showSpareParts: boolean = true;
  @Input() showServiceTypes: boolean = true;

  @Output() searchParams: JobCardRepairRequestSearchCriteria = new JobCardRepairRequestSearchCriteria();

  spareParts: any = [];
  serviceTypes: any = [];
  jobCardStatuses: any = [];

  constructor(
    private readonly sparePartService: SparePartService,
    private readonly serviceTypeService: ServiceTypeService,
    private readonly jobCardStatusService: JobCardStatusService,
    private readonly commonService: CommonService) {
  }

  ngOnInit() {

    if (this.showSpareParts) {
      this.getSparParts();
    }

    if (this.showServiceTypes) {
      this.getServiceTypes();
    }

    this.getJobCardStatuses();
  }

  private getSparParts(): void {

    this.sparePartService.getSpareParts().subscribe(
      sparePartsRes => {

        if (sparePartsRes["isSucceeded"] === true) {
          this.spareParts = sparePartsRes["data"];
        }
      });
  }

  private getServiceTypes(): void {

    this.serviceTypeService.getServiceTypes().subscribe(
      serviceTypeRes => {
        if (serviceTypeRes["isSucceeded"] === true) {
          this.serviceTypes = serviceTypeRes["data"];
        }
      });
  }

  private getJobCardStatuses(): void {
    this.jobCardStatusService.getJobCardStatuses().subscribe(
      statusRes => {
        if (statusRes["isSucceeded"] === true) {
          this.jobCardStatuses = statusRes["data"];
        }
      });
  }

  getQueryStringParams() {

    let customerNameParam = this.searchParams.customerName;

    if (customerNameParam === "0") {
      customerNameParam = "";
    }

    let sparePartIdParam: string = this.searchParams.sparePartId.toString();

    if (sparePartIdParam === "0") {
      sparePartIdParam = "";
    }

    let serviceTypeIdParam: string = this.searchParams.serviceTypeId.toString();

    if (serviceTypeIdParam === "0") {
      serviceTypeIdParam = "";
    }

    let startDateParam: string;

    if (this.searchParams.startDate) {
      startDateParam = this.commonService.getDateAsString(this.searchParams.startDate);
    } else {
      startDateParam = "";
    }

    let endDateParam: string;

    if (this.searchParams.endDate) {
      endDateParam = this.commonService.getDateAsString(this.searchParams.endDate);
    } else {
      endDateParam = "";
    }

    const vehiclePlateNoParam = this.searchParams.vehiclePlateNo;
    const vehicleFleetNoParam = this.searchParams.vehicleFleetNo;

    let jobCardIdParam = this.searchParams.jobCardId;

    if (jobCardIdParam == null) {
      jobCardIdParam = "";
    }

    let jobCardStatusIdParam: string = this.searchParams.jobCardStatusId.toString();

    if (jobCardStatusIdParam === "0") {
      jobCardStatusIdParam = "";
    }

    let queryStringParams: string =
      `startDate=${startDateParam}&endDate=${endDateParam}&vehiclePlateNo=${vehiclePlateNoParam}&vehicleFleetNo=${vehicleFleetNoParam}&jobCardId=${jobCardIdParam}&jobCardStatusId=${jobCardStatusIdParam}`;

    if (this.showCustomerName) {
      queryStringParams += `&customerName=${customerNameParam}`;
    }

    if (this.showServiceTypes) {
      queryStringParams += `&serviceTypeId=${serviceTypeIdParam}`;
    }

    if (this.showSpareParts) {
      queryStringParams += `&sparePartId=${sparePartIdParam}`;
    }

    return queryStringParams;
  }

  clearSearchParams() {
    this.searchParams.resetValues();
  }
}
