import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCardRepairRequestSearchCriteriaComponent } from './job-card-repair-request-search-criteria.component';

describe('JobCardRepairRequestSearchCriteriaComponent', () => {
  let component: JobCardRepairRequestSearchCriteriaComponent;
  let fixture: ComponentFixture<JobCardRepairRequestSearchCriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobCardRepairRequestSearchCriteriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCardRepairRequestSearchCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
