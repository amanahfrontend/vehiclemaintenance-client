import { TestBed } from '@angular/core/testing';

import { CannibalizationService } from './cannibalization.service';

describe('CannibalizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CannibalizationService = TestBed.get(CannibalizationService);
    expect(service).toBeTruthy();
  });
});
