import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";

@Injectable({
  providedIn: "root"
})
export class CannibalizationService {

  constructor(
    private readonly http: HttpClient,
    private readonly config: WebApiService) { }

  getCannibalizations(): Observable<any[]> {
    const url = this.config.baseUrl + "Cannibalization/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <any[]>data;
      return result;
    }));
  }

  addCannibalization(model): Observable<any> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Cannibalization/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const results = <any>data;
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

  updateCannibalization(model): Observable<any> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "Cannibalization/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const results = <any>data;
        return results;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }

}
