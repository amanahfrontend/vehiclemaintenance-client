import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { SparePart } from "../../spare-parts/models/spare-part.model";
import { SparePartService } from "../../spare-parts/services/spare-part.service";
import { TechnicianService } from "../../technicians/services/technician.service";
import { CannibalizationService } from "../services/cannibalization.service";

@Component({
  selector: "app-cannibalization-add-edit",
  templateUrl: "./cannibalization-add-edit.component.html",
  providers: [TechnicianService, SparePartService, CommonService]
})
export class CannibalizationAddEditComponent implements OnInit {
  model: any = {};
  technicians: any[];

  currentSparePart: SparePart;

  title: string = "";
  btnTitle: string = "";
  visible = false;
  visibleAnimate = false;
  spareParts: any[];

  constructor(
    private readonly cannibalizationService: CannibalizationService,
    private readonly technicianService: TechnicianService,
    private readonly sparePartService: SparePartService,
    public dialogRef: MatDialogRef<CannibalizationAddEditComponent>,
    private readonly customDialogService: DialogService,
    private readonly commonService: CommonService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.setComponentLabels();
    this.getTechnicians();
    this.getSparParts();

    this.initCanniModel();
  }

  private setComponentLabels() {

    if (this.data.id) {
      this.title = "Edit Cannibalization";
      this.btnTitle = "Update";
    } else {
      this.title = "Add Cannibalization";
      this.btnTitle = "Add";
    }
  }

  saveCannibalization() {
    const isValid = this.validateBeforeSavingCannibalization();

    if (!isValid) {
      return;
    }

    if (this.model.id) {

      this.cannibalizationService.updateCannibalization(this.model).subscribe(
        vehicleRes => this.showMessageAfterSave(vehicleRes));
    } else {

      this.cannibalizationService.addCannibalization(this.model).subscribe(
        vehicleRes => this.showMessageAfterSave(vehicleRes));
    }
  }

  validateBeforeSavingCannibalization() {
    if (this.model.vehicleFromFleetNo === this.model.vehicleToFleetNo) {
      this.customDialogService.alert("", "The target vehicle can't be the original one.", false);
      return false;
    }

    if (this.model.cannibalizationSpareParts.length === 0) {
      this.customDialogService.alert("", "Please specify the spare parts removed from the first vehicle.", false);
      return false;
    }

    if (this.model.technicianRefixedById === "0") {
      this.model.technicianRefixedById = null;
    }

    return true;
  }

  closeButtonClick(): void {
    this.dialogRef.close();
  }

  private showMessageAfterSave(responseResult): void {

    if (responseResult["isSucceeded"] === true) {
      //this.customDialogService.alert("Done", operation + " successfully", false);
      this.visibleAnimate = false;
      setTimeout(() => (this.visible = false), 300);

      this.printCannibalizationReport(responseResult.data.id);

      this.dialogRef.close();
      this.model = {};
    } else {

      const errorMessage = this.formulateErrorMessage(responseResult);
      this.customDialogService.alert("Fail", errorMessage, false);
    }
  }

  private formulateErrorMessage(responseResult) {
    let errorMessage = "Failed to save the record. ";

    if (responseResult.statusCode !== 0) {
      errorMessage += responseResult.message;
    } else {
      console.log(responseResult.message);
    }

    return errorMessage;
  }

  getTechnicians() {

    // Get Technicians
    this.technicianService.getTechnicians().subscribe(
      technicianRes => {
        if (technicianRes["isSucceeded"] === true) {
          this.technicians = technicianRes["data"];
        }
      });
  }

  private getSparParts(): void {

    this.sparePartService.getSpareParts().subscribe(
      sparePartsRes => {

        if (sparePartsRes["isSucceeded"] === true) {
          this.spareParts = sparePartsRes["data"];
        }
      });
  }

  addSparePartToCanniModel() {

    if (this.currentSparePart == undefined || this.currentSparePart.id === 0) {
      return;
    }

    this.model.cannibalizationSpareParts.push({
      sparePartId: this.currentSparePart.id,
      sparePartName: this.currentSparePart.nameEn
    });

    this.currentSparePart = null;
  }

  deleteSparePartFromCanniModel(index: number) {
    this.model.cannibalizationSpareParts.splice(index, 1);
  }

  initCanniModel() {
    this.model.technicianRefixedById = 0;
    this.model.cannibalizationSpareParts = [];
  }

  printCannibalizationReport(cannId: number) {
    this.commonService.print2(10, `cannId=${cannId}`);
  }
}
