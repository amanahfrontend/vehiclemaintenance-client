import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CannibalizationAddEditComponent } from "./cannibalization-add-edit.component";

describe("CannibalizationAddEditComponent", () => {
  let component: CannibalizationAddEditComponent;
  let fixture: ComponentFixture<CannibalizationAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CannibalizationAddEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CannibalizationAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
