import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { CommonService } from "../../shared/services/common.service";
import { CannibalizationAddEditComponent } from "../add-edit/cannibalization-add-edit.component";
import { CannibalizationService } from "../services/cannibalization.service";

@Component({
  selector: "app-cannibalization-list",
  templateUrl: "./cannibalization-list.component.html",
  providers: [CannibalizationService, CommonService]
})
export class CannibalizationListComponent implements OnInit {
  cannibalizations: any = [];
  showLoading: boolean = false;
  showNoDataFoundLabel: boolean = false;

  constructor(
    private readonly cannibalizationService: CannibalizationService,
    private readonly commonService: CommonService,
    private readonly dialog: MatDialog) { }

  ngOnInit() {
  }

  searchCannibalizations() {
    this.showLoading = true;

    this.cannibalizationService.getCannibalizations().subscribe(
      responseResult => {
        if (responseResult && responseResult["isSucceeded"] === true) {
          this.cannibalizations = responseResult["data"];

          this.showLoading = false;
        }
      });
  }

  openAddCannibalizationDialog(): void {

    const dialogOptions = {
      width: "850px",
      height: "600px",
      data: { id: 0 }
    };

    const dialogRef = this.dialog.open(CannibalizationAddEditComponent, dialogOptions);

    dialogRef.afterClosed().subscribe(() => {
      this.searchCannibalizations();
    });
  }

  printCannibalizationReport(cannId: number) {
    this.commonService.print2(10, `cannId=${cannId}`);
  }
}
