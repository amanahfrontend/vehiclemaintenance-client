import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CannibalizationListComponent } from "./cannibalization-list.component";

describe("CannibalizationListComponent", () => {
  let component: CannibalizationListComponent;
  let fixture: ComponentFixture<CannibalizationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CannibalizationListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CannibalizationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
