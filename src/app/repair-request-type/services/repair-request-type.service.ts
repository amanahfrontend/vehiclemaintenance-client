import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WebApiService } from "../../shared/services/web-api.service";
import { RepairRequestType } from "../models/repair-request-type.model";

@Injectable()
export class RepairRequestTypeService {

  constructor(private http: HttpClient, private config: WebApiService) { }

  getRepairRequestTypes(): Observable<RepairRequestType[]> {
    const url = this.config.baseUrl + "RepairRequestType/Get";

    return this.http.get(url).pipe(map(data => {
      const result = <RepairRequestType[]>data;
      return result;
    }));
  }

  addRepairRequestType(model): Observable<RepairRequestType> {
    model.rowStatusId = 1;
    model.createdByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestType/Add";

    return this.http.post(url, model).pipe(map(
      data => {
        const result = <RepairRequestType>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  updateRepairRequestType(model): Observable<RepairRequestType> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.config.currentUserId;

    const url = this.config.baseUrl + "RepairRequestType/Update";

    return this.http.put(url, model).pipe(map(
      data => {
        const result = <RepairRequestType>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

  deleteRepairRequestType(id: number) {
    let url = "{0}RepairRequestType/Delete/{1}/{2}";

    url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
      .replace("{2}", this.config.currentUserId);

    return this.http.delete(url).pipe(map(
      data => {
        const result = <RepairRequestType>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }
}
