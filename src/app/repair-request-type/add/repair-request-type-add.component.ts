import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RepairRequestTypeService } from "../services/repair-request-type.service";
import { DialogService } from "../../shared/services/dialog.service";

@Component({
    selector: 'app-repair-request-type-add',
    templateUrl: './repair-request-type-add.component.html',
    providers: [RepairRequestTypeService]
})
export class RepairRequestTypeAddComponent implements OnInit {
    public visible = false;
    public visibleAnimate = false;
    model: any = {};

    constructor(
        private repairRequestTypeService: RepairRequestTypeService,
        private customDialogService: DialogService,
        private dialogRef: MatDialogRef<RepairRequestTypeAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
        this.model.needApprove = false;
    }

    closeButtonClick(): void {
        this.dialogRef.close();
    }

    saveRepairRequestType() {

        this.repairRequestTypeService.addRepairRequestType(this.model).subscribe(
            repairRequestTypeRes => {
                if (repairRequestTypeRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                    this.dialogRef.close();
                }
                else {
                    const errorMessage = this.formulateErrorMessage(repairRequestTypeRes);
                    this.customDialogService.alert("Fail", errorMessage, false);
                }
            });
    }

    private formulateErrorMessage(responseResult) {
        let errorMessage = "Failed to save the record. ";

        if (responseResult.statusCode !== 0) {
            errorMessage += responseResult.message;
        } else {
            console.log(responseResult.message);
        }

        return errorMessage;
    }
}

