import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { RepairRequestType } from "../models/repair-request-type.model";
import { DialogService } from "../../shared/services/dialog.service";
import { RepairRequestTypeService } from "../services/repair-request-type.service";
import { RepairRequestTypeAddComponent } from "../add/repair-request-type-add.component";

@Component({
    selector: "app-repair-request-type-list",
    templateUrl: "./repair-request-type-list.component.html",
    styleUrls: ["./repair-request-type-list.component.css"],
    providers: [RepairRequestTypeService]
})
export class RepairRequestTypeListComponent implements OnInit {
    repairRequestTypes: RepairRequestType[];
    isActive: boolean = false;

    constructor(
        private repairRequestTypeService: RepairRequestTypeService,
        private customDialogService: DialogService,
        public dialog: MatDialog) { }

    ngOnInit() {
        this.getAllRepairTypes();
    }

    getAllRepairTypes() {
        this.isActive = true;

        this.repairRequestTypeService.getRepairRequestTypes().subscribe(
            repairRequestTypeRes => {

                if (repairRequestTypeRes["isSucceeded"] === true) {
                    this.repairRequestTypes = repairRequestTypeRes["data"];

                    this.isActive = false;
                }
            });
    }

    openAddRepairTypes(): void {

        const dialogOptions = {
            width: "600px",
            data: { id: 0 }
        };

        let dialogRef = this.dialog.open(RepairRequestTypeAddComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getAllRepairTypes();
        });
    }

    deleteRepairRequestType(id) {
        const question = "Do you really want to delete this record?";
        const result = this.customDialogService.confirm("Confirm", question, true);

        result.subscribe(
            (res) => {
                if (res) {

                    this.repairRequestTypeService.deleteRepairRequestType(id).subscribe(
                        repairRequestTypeRes => {

                            if (repairRequestTypeRes["isSucceeded"] === true) {

                                this.getAllRepairTypes();
                            }
                        });
                }
            },
            () => {
                console.log("declined");
            }
        );
    }
}