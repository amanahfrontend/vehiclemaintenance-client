import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { RepairRequestTypeService } from "../services/repair-request-type.service";
import { DialogService } from "../../shared/services/dialog.service";

@Component({
    selector: "app-repair-request-type-edit",
    templateUrl: "./repair-request-type-edit.component.html",
    styleUrls: ["./repair-request-type-edit.component.css"],
    providers: [RepairRequestTypeService]
})
export class RepairRequestTypeEditComponent implements OnInit {

    visible = false;
    visibleAnimate = false;
    model: any = {};
    @Output() dialogClosed = new EventEmitter();

    constructor(
        private repairRequestTypeService: RepairRequestTypeService,
        private customDialogService: DialogService) {
    }

    ngOnInit() { }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    showDialog(model): void {

        Object.assign(this.model, model);

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    updateRepairRequestType() {

        this.repairRequestTypeService.updateRepairRequestType(this.model).subscribe(
            repairRequestTypeRes => {
                if (repairRequestTypeRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                    //this.customDialogService.alert("Done", "updated successfully", false);

                    this.dialogClosed.emit();

                } else {
                    const errorMessage = this.formulateErrorMessage(repairRequestTypeRes);

                    this.customDialogService.alert("Fail", errorMessage, false);
                }
            });
    }

    private formulateErrorMessage(responseResult) {
        let errorMessage = "Failed to save the record. ";

        if (responseResult.statusCode !== 0) {
            errorMessage += responseResult.message;
        } else {
            console.log(responseResult.message);
        }

        return errorMessage;
    }
}

