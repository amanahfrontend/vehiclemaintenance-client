export class RepairRequestType {
    id: string;
    nameAr: string;
    nameEn: string;
    needApprove: boolean;
}